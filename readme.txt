This framework is meant to enable the quick parallelisation of 
single-core TinyOS projects into a multi-core solution running 
on a queue-based messaging platform.