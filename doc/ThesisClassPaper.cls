% Thesis class definition for Vanderbilt Univesity's Graduate School guidelines.
% Written by Will Hedgecock, Spring 2012

% Declare package
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ThesisClassPaper}[2013/05/01 Vanderbilt University's Thesis Class]

% Pass default options
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions\relax
\LoadClass{report}

% Include necessary packages
\RequirePackage[letterpaper,left=1in,top=1in,right=1in,bottom=1in,nohead,footskip=0.5in]{geometry}
\RequirePackage{indentfirst}
\RequirePackage{float}
\RequirePackage{graphicx}
\RequirePackage{setspace}
\RequirePackage{listings}
\RequirePackage{chngcntr}
\RequirePackage[cmex10]{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{url}
\RequirePackage{subfigure}
\RequirePackage{ifthen}
\RequirePackage{nomencl}
\makenomenclature
\errorcontextlines 999

% See if hyperref package was loaded by user
\AtBeginDocument{\@ifpackageloaded{hyperref}{}{\def\hypertarget#1{}}}

% Set doublespacing and place captions in correct place
\floatstyle{plain}
\restylefloat{figure}
\newcommand{\doublespacesize}{1.66}
\newcommand{\singlespacesize}{1}
\renewcommand{\baselinestretch}{1}

% No whitespace above or below formulas
\expandafter\def\expandafter\normalsize\expandafter
{\normalsize\setlength\abovedisplayskip{1pt}
 \setlength\abovedisplayshortskip{1pt}
 \setlength\belowdisplayskip{1pt}
 \setlength\belowdisplayshortskip{1pt}}

% Print chapter number in roman numerals and set numbers
\renewcommand{\thechapter}{\Roman{chapter}}
\renewcommand{\thesection}{\Roman{chapter}.\arabic{section}}
\renewcommand{\theequation}{\arabic{chapter}.\arabic{equation}}
\setcounter{secnumdepth}{1}
\setcounter{tocdepth}{3}
\counterwithout{table}{chapter}
\counterwithout{figure}{chapter}
\renewcommand\@seccntformat[1]{}

% Set the names of these areas to be what we want, all caps 
\renewcommand{\contentsname}{TABLE OF CONTENTS}
\renewcommand{\listfigurename}{LIST OF FIGURES}
\renewcommand{\listtablename}{LIST OF TABLES}
\renewcommand{\nomname}{LIST OF ABBREVIATIONS}
\renewcommand{\bibname}{REFERENCES}
\renewcommand{\chaptername}{CHAPTER}
\renewcommand{\appendixname}{APPENDIX}

% Setup margins
\pdfpagewidth 8.5in
\pdfpageheight 11in
\widowpenalty=10000
\brokenpenalty=10000

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Redefine the macro used for floats so that single spacing is used
%\def\@xfloat#1[#2]{
    %\ifhmode \@bsphack\@floatpenalty -\@Mii
    %\else \@floatpenalty-\@Miii\fi
    %\def\@captype{#1}
    %\ifinner \@parmoderr\@floatpenalty\z@
    %\else \@next\@currbox\@freelist{\@tempcnta\csname ftype@#1\endcsname
        %\multiply\@tempcnta\@xxxii\advance\@tempcnta\sixt@@n
        %\@tfor \@tempa :=#2\do
        %{
            %\if\@tempa h\advance\@tempcnta \@ne\fi
            %\if\@tempa t\advance\@tempcnta \tw@\fi
            %\if\@tempa b\advance\@tempcnta 4\relax\fi
            %\if\@tempa p\advance\@tempcnta 8\relax\fi
        %}
        %\global\count\@currbox\@tempcnta}\@fltovf
    %\fi
    %\global\setbox\@currbox\vbox\bgroup 
    %\def\baselinestretch{1}\@normalsize
    %\boxmaxdepth\z@
    %\hsize\columnwidth \@parboxrestore
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redefine the macro used for footnotes to use single spacing
\long\def\@footnotetext#1{
    \insert\footins
	{
	    \def\baselinestretch{1}\footnotesize
		\interlinepenalty\interfootnotelinepenalty 
		\splittopskip\footnotesep
		\splitmaxdepth \dp\strutbox \floatingpenalty \@MM
		\hsize\columnwidth \@parboxrestore
		\edef\@currentlabel{\csname p@footnote\endcsname\@thefnmark}\@makefntext
		{\rule{\z@}{\footnotesep}\ignorespaces
		#1\strut}
	}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define document-wide information macros
\def\department#1{\gdef\@department{#1}}
\def\advisor#1{\gdef\@advisor{#1}}
\def\firstreader#1{\gdef\@firstreader{#1}}
\def\secondreader#1{\gdef\@secondreader{#1}}
\def\thirdreader#1{\gdef\@thirdreader{#1}}
\def\fourthreader#1{\gdef\@fourthreader{#1}}
\def\fifthreader#1{\gdef\@fifthreader{#1}}
\def\submitdate#1{\gdef\@submitdate{#1}}
\def\copyrightyear#1{\gdef\@copyrightyear{#1}}
\def\@title{}\def\@author{}\def\@department{}
\def\@advisor{}
\def\@firstreader{}\def\@secondreader{}\def\@thirdreader{}
\def\@fourthreader{}\def\@fifthreader{}
\def\@submitdate{\ifcase\the\month\or
  January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or December\fi
  ,\space \number\the\year}
\ifnum\month=12
    \@tempcnta=\year \advance\@tempcnta by 1
    \edef\@copyrightyear{\number\the\@tempcnta}
\else
    \def\@copyrightyear{\number\the\year}
\fi\vskip 0.5in
\newif\ifcopyright
\copyrighttrue \vskip 0.5in
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define title page format and creation macro
\def\titlepage{{%
    \thispagestyle{empty}%
    \begin{center}
	    \doublespacing
	    \uppercase\expandafter{\@title}\\ \vskip 0.1in \vfil
        By\\ \vskip 0.1in \vfil
        \@author\\ \vskip 0.1in \vfil
        Department of \@department\\Vanderbilt University\\Nashville, Tennessee \vskip 0.1in \vfil
        \@submitdate
    \end{center}
    \newpage}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define copyright page format
\def\copyrightpage{%
    \thispagestyle{empty}%
	\vfill
	\begin{center}
	    \copyright\ Copyright by \@author \ \@copyrightyear \\
		All Rights Reserved
	\end{center}
	\vfill\newpage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define preliminary pages 
\def\maketitlepages{
    \pagenumbering{roman}
	\pagestyle{plain}
	\titlepage
	\ifcopyright\copyrightpage\fi
	\addtocontents{toc}{\hfill Page \vskip 0.01em}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Include a preface section before the table of contents
\newcommand{\prefacechapter}[1]
{
    \chapter*{#1}
	\addcontentsline{toc}{chapter}{\protect\uppercase{#1}}
	\doublespacing
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finish making all preliminary pages including table of contents and lists
% Modified - Ben 2013-03-20
\def\afterpreface{
	\begin{spacing}{\singlespacesize}
	\newpage \tableofcontents \newpage
	\stepcounter{chapter}\addcontentsline{toc}{chapter}{\listtablename} \addtocontents{lot}{\protect\flushleft Table \hfill Page} \addtocontents{lot}{} \listoftables \newpage
	\stepcounter{chapter}\addcontentsline{toc}{chapter}{\listfigurename} \addtocontents{lof}{\protect\flushleft Figure \hfill Page} \addtocontents{lof}{} \listoffigures \newpage
%	\stepcounter{chapter}\addcontentsline{toc}{chapter}{\nomname} \printnomenclature[4cm] \newpage
	\addtocontents{toc}{\protect\flushleft \vskip -0.1em Chapter} %\pagenumbering{arabic} \pagestyle{plain}
	\end{spacing}
	\setcounter{chapter}{0}
	\pagenumbering{arabic}
	\pagestyle{plain}
	\doublespacing}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Redefine \thebibliography to go to a new page and put an entry in the table of contents
\let\@ldthebibliography\thebibliography
\renewcommand{\thebibliography}[1]{\newpage \addcontentsline{toc}{chapter}{REFERENCES} \singlespacing
    \@ldthebibliography{#1}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abstract environment definition
\renewenvironment{abstract}{ 
	      \bgroup 
              \newpage
              \newcounter{savepageno}
              \setcounter{savepageno}{\value{page}}
              \thispagestyle{empty}
 	      \pagestyle{empty}
	      \hfill\normalsize\rm\underline{ELECTRICAL ENGINEERING} 
	      \vspace{1.5\baselineskip} 
              \begin{center}
                 \doublespacing
                 \normalsize\uppercase\expandafter{\@title}\\
                 \vspace{1.0\baselineskip}
                 \uppercase\expandafter{\@author}
              \end{center}
          \vspace{1.5\baselineskip}
   \begin{spacing}{1}
   \centerline{\underline{Thesis under the direction of Dr. \@principaladvisor}}\par 
   \vspace{0.5\baselineskip}
   \end{spacing}
   \doublespacing
} { 
   {\vfill \vfill \vspace{0.2in}
   \hskip -0.3in Approved\,\rule[-1mm]{3.4in}{0.4pt} \hfill \ Date\,\rule[-1mm]{1.5in}{0.4pt}\\
   \par\newpage\setcounter{page}{\value{savepageno}}\egroup}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Single-space quote and quotation environments
\renewenvironment{quotation}
    {\list{}{\listparindent 1.5em%
	\itemindent    \listparindent
	\rightmargin   \leftmargin
	\parsep        \z@ \@plus\p@}%
	\begin{spacing}{\singlespacesize} \item\relax}
	{\end{spacing}\endlist}
\renewenvironment{quote}
    {\list{}{\rightmargin\leftmargin}%
	\begin{spacing}{\singlespacesize} \item\relax}
	{\end{spacing}\endlist}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setup correct Chapter heading formats
\renewcommand\@makechapterhead[1]{%
    {\parindent \z@ \centering \normalfont
     \ifnum \c@secnumdepth >\m@ne
		\ifthenelse{\equal{\chaptername}{\appendixname}}
			{\hypertarget{\chaptername.\thechapter}}{\hypertarget{\chaptername.\arabic{chapter}}}
        \bfseries \uppercase{\chaptername}\space\thechapter\vspace{6ex}
		\par\nobreak
     \fi
     \interlinepenalty\@M
     \bfseries \uppercase{#1}\par\nobreak \vspace{6ex}
    }
}

\renewcommand\@makeschapterhead[1]{%
\begin{spacing}{1}
    {\parindent \z@ \centering \normalfont
     \interlinepenalty\@M
	 \ifthenelse{\value{chapter}>0}{\hypertarget{\chaptername*.\arabic{chapter}}}{}
     \bfseries \uppercase{#1}\par\nobreak \vspace{4ex}
    }
\end{spacing}
}

\renewcommand\section{\@startsection{section}{1}{\z@}%
  {2em}{0.1em}%
  {\centering \normalfont \normalsize \bf%
  \ifnum\value{section}>1 \else \addtocontents{toc}{\protect\addvspace{1em}}\fi}}
\renewcommand\subsection{\vskip 2em \@startsection{subsection}{2}{\z@}%
  {0em}{0.1em}%
  {\raggedright \normalfont \normalsize \bf\itshape}}
\renewcommand\subsubsection{\vskip 2em \@startsection{subsubsection}{3}{\z@}%
  {0.1em}{0.1em}%
  {\raggedright \normalfont \normalsize \itshape}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fix Table of Contents format
\def\l@chapter#1#2{\pagebreak[3]
    \vskip 1em plus 1pt
	\@dottedtocline{0}{0em}{3em}{\uppercase{#1}}{#2}
}

\def\@dottedtocline#1#2#3#4#5{%
  \ifnum #1>\c@tocdepth \else
    \vskip \z@ \@plus.2\p@
    {\ifnum #1=1 \def\numberline##1{\hfil}\fi%
     \leftskip #2\relax \rightskip \@tocrmarg \parfillskip -\rightskip
     \parindent #2\relax\@afterindenttrue
     \interlinepenalty\@M
     \leavevmode
     \@tempdima #3\relax
     \advance\leftskip \@tempdima \null\nobreak\hskip -\leftskip
     {#4}\nobreak
     \leaders\hbox{$\m@th
        \mkern \@dotsep mu\hbox{.}\mkern \@dotsep
        mu$}\hfill
     \nobreak
     \hb@xt@\@pnumwidth{\hfil\normalfont \normalcolor #5}%
     \par}%
  \fi}

% Print sub-chapter lines indented appropriately
\renewcommand\l@section{\@dottedtocline{1}{4.5em}{2.4em}}
\renewcommand\l@subsection{\@dottedtocline{2}{6em}{3.2em}}
\renewcommand\l@subsubsection{\@dottedtocline{3}{7.5em}{4.1em}}
\renewcommand\l@paragraph{\@dottedtocline{4}{13.5em}{5em}}
\renewcommand\l@subparagraph{\@dottedtocline{5}{16em}{6em}}

% Put a blank line above figure and table entries in their lists
\renewcommand{\l@figure}{
	\protect\flushleft
	\@dottedtocline{0}{1.5em}{3.3em}}
\renewcommand{\l@table}{\l@figure}

% Put a period after the item number in the table of contents
\def\numberline#1{\hb@xt@\@tempdima{{#1.}\hfil}}

% Redefine \appendix to put an entry in the table of contents
\let\@ldappendix\appendix
\renewcommand{\appendix}{
    \@ldappendix
	\addtocontents{toc}{\protect\flushleft \vskip -0.1em Appendix}
	\renewcommand{\chaptername}{APPENDIX}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
