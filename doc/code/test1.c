/*
 * test1.c
 *
 * Created: 1/17/2013 13:16:42
 *  Author: babjak
 */ 


#include <avr/io.h>

#include <stdlib.h>
#include <string.h>


#define CORE1	0
#define CORE2	1
#define CORE3	2

#define RF_service_comp  1


/************************************************************************/
/* Memory mapped HW                                                     */
/************************************************************************/
#define ADC_SPI_REG_ADDR			(uint8_t*)0x1234
#define RF_SPI_REG_ADDR				(uint8_t*)0x1234

volatile uint8_t* const ADC_SPI_reg		= ADC_SPI_REG_ADDR;
volatile uint8_t* const RF_SPI_reg		= RF_SPI_REG_ADDR;


#define TASK_BUFF_SIZE				32

#define TASK_BUFF_HW_MALLOC_ADDR	(uint8_t*)0x1234 
#define TASK_BUFF_POOL_BASE_ADDR	(uint8_t*)0x1234
#define TASK_WR_ADDR				(uint8_t*)0x1234
#define TASK_WR_STATUS_ADDR			(uint8_t*)0x1234
#define TASK_BUFF_MALLOC_FAIL		0xFF

volatile uint8_t* const task_buff_hw_malloc	= TASK_BUFF_HW_MALLOC_ADDR;
uint8_t* const			task_buff_pool_base	= TASK_BUFF_POOL_BASE_ADDR;
volatile uint8_t* const	task_wr				= TASK_WR_ADDR;
volatile uint8_t* const task_wr_status		= TASK_WR_STATUS_ADDR; //1 last write success, 0 write has not finished yet


#define TASK_FIFO_FRONT_ADDR		(uint8_t*)0x1234
#define TASK_FIFO_CTRL_ADDR			(uint8_t*)0x1234

volatile uint8_t* const task_FIFO_front		= TASK_FIFO_FRONT_ADDR;
volatile uint8_t* const task_FIFO_ctrl		= TASK_FIFO_CTRL_ADDR;


/************************************************************************/
/* Task (function) management                                           */
/************************************************************************/
typedef void (*async_fun_t)(uint8_t* const state_buff, uint8_t* const param_buff);


uint8_t async_exec(const uint8_t			core,
					async_fun_t const		p_fun,
					uint8_t const			state_ID, //lower 4 bits used only
					uint8_t* const			param_buff)
{
	//1. Allocate memory for params
	const uint8_t	task_buff_pool_slot = *task_buff_hw_malloc; //Sets the right task_wr_status bit 0
	if (task_buff_pool_slot == TASK_BUFF_MALLOC_FAIL)
		return 1;

	//2. Copy task into allocated buffer
	const uint8_t mem_offset = task_buff_pool_slot < 5;
	uint8_t* const task_buff = task_buff_pool_base + mem_offset;
	
	uint16_t const fun_ID = (((uint16_t) p_fun) << 4) + ((uint16_t) (state_ID & 0xF));
	
	memcpy(task_buff, &fun_ID, 2);
	memcpy(task_buff+2, param_buff, 30);
	
	
	//2. Initiate transfer to executing core's task FIFO
	*task_wr = mem_offset + (core & 0x1F);


	//3. Make sure transfer succeeded - potential deadlock!
	while( !( ((*task_wr_status) > task_buff_pool_slot) & 0x1 ) )
	{}
	 
	return 0;
}


uint8_t* const get_state_buff_from_ID(uint8_t const state_ID)
{
//	uint8_t* state_buff;
	
	//TODO
	//If state is not in list, malloc, initialize to 0
	//If state is in list find memory address
	
	//list can be simple or more sophisticated, B-tree... (probably overkill)
	
//	return state_buff;
	return (uint8_t*) NULL;
}


uint8_t start_next_task()
{
	//1. Pop current task from FIFO
	*task_FIFO_ctrl = 1;
	
	//2. Check FIFO front
	uint16_t* p_fun_ID = (uint16_t*) task_FIFO_front;
	
	if ( !(*p_fun_ID) )
		return 1;
		
	//3. Start valid task
	uint8_t const	state_ID	= (uint8_t) ((*p_fun_ID) & 0xF);	
	async_fun_t p_async_fun		= (async_fun_t) ((*p_fun_ID) >> 4);
	uint8_t* const	param_buff	= task_FIFO_front + 2;

	uint8_t* const state_buff = get_state_buff_from_ID(state_ID);
	if ( state_ID && !state_buff )
		return 1;
		
	p_async_fun(state_buff, param_buff);
	
	return 0;
}


/************************************************************************/
/* Helper functions					                                    */
/************************************************************************/
void SPI_BitBang_read(volatile uint8_t* const	SPI_reg,
						uint8_t* const			p_data_in)
{
	//Turn off interrupt
	//Bang the bit
	//Turn on interrupt
}


void SPI_BitBang_write(volatile uint8_t* const	SPI_reg,
						uint8_t const			data_out)
{
	//Turn off interrupt
	//Bang the bit
	//Turn on interrupt
}


/************************************************************************/
/* Async functions (potential tasks)                                    */
/************************************************************************/
void SendRFmsg(uint8_t* const state_buff, uint8_t* const param_buff)
{
	uint8_t* msg = param_buff;
	
	uint32_t* p_send_counter = (uint32_t*) state_buff;
	(*p_send_counter) += 1;
	
	for (unsigned char i = 0; i<10; i++)
	{
		SPI_BitBang_write( ADC_SPI_reg, *(msg + i) );
	}	
}


void SignalProcessing(uint8_t* const state_buff, uint8_t* const param_buff)
{
	//uint8_t ADC_data_in = *param_buff;
	
	//Processing
	
	uint8_t msg[10];
	
	async_exec(CORE2, SendRFmsg, RF_service_comp, (uint8_t*) msg); 
}


void RFSignalProcessing(uint8_t* const state_buff, uint8_t* const param_buff)
{
	//uint8_t RF_data_in = *param_buff;
	
	//Processing
	
	uint8_t msg[10];
	
	async_exec(CORE2, SendRFmsg, RF_service_comp, (uint8_t*) msg);
}


/************************************************************************/
/* Interrupt handler functions	                                        */
/************************************************************************/
void interrupt_HW_clock()
{
	uint8_t data_in;
	
	SPI_BitBang_read( ADC_SPI_reg, &data_in );
	async_exec(CORE3, SignalProcessing, 0, &data_in);
}


void interrupt_RF()
{
	uint8_t data_in;
	
	SPI_BitBang_read( RF_SPI_reg, &data_in );
	async_exec(CORE3, RFSignalProcessing, 0, &data_in); 
}


/************************************************************************/
/* Main							                                        */
/************************************************************************/
int main(void)
{
    while(1)
    {
        start_next_task();
    }
}