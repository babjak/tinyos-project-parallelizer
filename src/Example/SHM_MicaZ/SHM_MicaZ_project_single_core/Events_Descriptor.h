#if defined(EVENTS_DESCRIPTOR_H)
#else
#define EVENTS_DESCRIPTOR_H

#define NUMBER_OF_EVENT_CHANNELS	2

typedef struct Event_s
{
    uint32_t	frame_time_ms;	
    int16_t		log_quality_idx_m[NUMBER_OF_EVENT_CHANNELS];	// milli log quality idx  
    int16_t		time_diff_us[NUMBER_OF_EVENT_CHANNELS-1];		// time difference of event relative to event time on first channel
} Event_t;

#endif