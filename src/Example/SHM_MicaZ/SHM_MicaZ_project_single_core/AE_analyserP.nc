#include "Events.h"
#include "Events_Descriptor.h"
#define ARRAYSIZE(x) (sizeof(x)/sizeof(*x))
#include <math.h>

#define N 100

generic module AE_analyserP()
{
    uses       interface Boot;

    uses       interface Timer<TMilli>    as Timer;

	provides   interface Read<Event_t>    as output;
}
implementation
{
    volatile uint16_t event_idx;

    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Process_AE()
    {
        error_t             result;
        uint16_t            i,j;
        volatile uint16_t   temp1, temp2;

        printStr("Process_AE - New event proc");

        //simulate AE processing
        for (j = 0; j < 2; j++)
        {
            // store squares
            for (i=0; i<N; i++)
                temp1 = temp1 * temp2;

            // calculate square sums
            for (i=0; i<N-1; i++)
                temp2 = temp1 + temp2;

            temp1 = 0; // min val
            temp2 = 1214; // min idx

            for (i=0; i<N; i++)
            {
                // calculate variances
                temp1 = ( temp1 + temp2 ) / ( temp1 - 1 );
                temp2 = ( temp2 - temp1 ) / ( temp2 - 1 );

                //calc cost function
                temp1 = temp1 / temp2 + temp2 / temp1;

                if ( temp1 > temp2 )
                {
                    temp1 = temp2;
                    temp2 = temp1;
                }
            }

            temp1 = log(temp2);

        }

        //time diff
        temp2 = temp1 - temp2;


        result = SUCCESS;

        printStr("Process_AE - Proc done");

        signal output.readDone( SUCCESS, events[event_idx] );
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Setup_Next_Event()
    {
        uint32_t    now_ms;
        uint16_t    i;

        now_ms = call Timer.getNow();

        for ( i = 0; i < ARRAYSIZE(events); i++ )
        {
            if ( now_ms >= events[i].frame_time_ms )
                continue;

            event_idx = i;

            call Timer.startOneShot( events[i].frame_time_ms - now_ms );
        }
    }


    event void Boot.booted()
    {
//        printStr("AE_analyserP Boot.booted()");
        Setup_Next_Event();
    }


    event void Timer.fired()
    {
        Process_AE();
        Setup_Next_Event();
    }  


    command error_t output.read()
    {
        return SUCCESS;
    }
}
