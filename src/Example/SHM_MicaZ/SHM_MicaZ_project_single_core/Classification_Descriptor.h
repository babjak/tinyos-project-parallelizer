#if defined(CLASSIFICATION_DESCRIPTOR_H)
#else
#define CLASSIFICATION_DESCRIPTOR_H

#include "Events_Descriptor.h"

#define DATA_DIMENSION      NUMBER_OF_EVENT_CHANNELS


typedef uint8_t point_cnt_t;

typedef struct classification_msg_s
{
    float       mean[DATA_DIMENSION];           // 4 byte * 2 dim = 8  
    float       variance[DATA_DIMENSION];       // 4 byte * 2 dim = 8
    point_cnt_t cluster_size;                   // 1 byte         = 1
} classification_msg_t;                         //                = 17

#endif
