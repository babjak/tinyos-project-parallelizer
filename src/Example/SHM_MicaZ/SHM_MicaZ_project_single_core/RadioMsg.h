#if defined(RADIO_MSG_H)
#else
#define RADIO_MSG_H

#define RADIO_MSG_BUFFER_SIZE 20

typedef nx_struct RadioMsg
{
    nx_uint16_t nodeid;
    nx_uint8_t	buffer[RADIO_MSG_BUFFER_SIZE];
//    nx_uint8_t	buffer[27];
} RadioMsg;

#endif