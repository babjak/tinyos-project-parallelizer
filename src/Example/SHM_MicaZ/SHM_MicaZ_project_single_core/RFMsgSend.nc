interface RFMsgSend
{
	command	void SendRadioMsg( uint16_t nodeid, void* buffer, uint8_t buffer_size );
}
