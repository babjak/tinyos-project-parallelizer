#include <Timer.h>

enum 
{
	AM_RADIO	= 6
};

configuration SenseForwardAppC
{
}
implementation
{
	components MainC;
	components SenseForwardP;

	components new TimerMilliC()	as Timer;
	components new DemoSensorC()	as Sensor;
//	components new ConstantSensorC(uint16_t, 0xBEEF) as Sensor;

	components SignalProcessingP;
	components RFProcessingP;

	components ActiveMessageC;
	components new AMSenderC(AM_RADIO);
	components new AMReceiverC(AM_RADIO);
	components AMSenderIFP;

	SenseForwardP.Boot			-> MainC;
	SenseForwardP.Timer			-> Timer;
	SenseForwardP.Read			-> Sensor;

	SenseForwardP.SignalProcess	-> SignalProcessingP.Process;
	SenseForwardP.RFProcess		-> RFProcessingP.Process;

	//Radio TX
	AMSenderIFP.Packet			-> AMSenderC;
//	SenseForwardP.AMPacket		-> AMSenderC;
	AMSenderIFP.AMSend			-> AMSenderC;
	SenseForwardP.RFMsgSend		-> AMSenderIFP;

	SenseForwardP.AMControl		-> ActiveMessageC;

	//Radio RX
	SenseForwardP.Receive		-> AMReceiverC;
}

