#if defined(RADIO_MSG_H)
#else
#define RADIO_MSG_H

typedef nx_struct RadioMsg
{
    nx_uint16_t nodeid;
    nx_uint16_t val;
} RadioMsg;

#endif