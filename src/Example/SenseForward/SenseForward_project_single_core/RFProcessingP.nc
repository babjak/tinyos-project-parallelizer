module RFProcessingP
{
    provides interface Process;

}
implementation
{
	uint16_t valt;

	task void processData()
	{
		signal Process.procDone(SUCCESS, valt);
	}

	command	error_t Process.proc( uint16_t val )
	{
		valt = val;
		post processData();
		return SUCCESS;
	}
}