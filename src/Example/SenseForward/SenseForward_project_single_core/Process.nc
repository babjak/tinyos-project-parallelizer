interface Process
{
    event	void	procDone( error_t err, uint16_t val );
	command	error_t	proc( uint16_t val );
}
