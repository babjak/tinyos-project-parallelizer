#include "RadioMsg.h"

module AMSenderIFP 
{
	provides interface RFMsgSend;

    //Radio TX
    //AMSenderC
    uses interface Packet;
    //uses interface AMPacket;
    uses interface AMSend;
}
implementation
{
    bool        busy   = FALSE;
    message_t   pkt;
  
    command	void RFMsgSend.SendRadioMsg( uint16_t nodeid, uint16_t val )
    {
		RadioMsg* rpkt;

        if (busy) 
            return;
            
        rpkt = (RadioMsg*) ( call Packet.getPayload(&pkt, sizeof (RadioMsg)) );
        rpkt->nodeid    = nodeid;
        rpkt->val       = val;
            
        if ( call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(RadioMsg)) == SUCCESS ) 
            busy = TRUE;
    }

    event void AMSend.sendDone(message_t* msg, error_t error)
    {
        if (&pkt == msg) 
            busy = FALSE;
    }    
   
}
