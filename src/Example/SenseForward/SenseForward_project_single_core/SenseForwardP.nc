#include "RadioMsg.h"

#define TIMER_PERIOD_MILLI  1000
#define NODE_ID             1


module SenseForwardP 
{
    uses interface Boot;
    
    uses interface Timer<TMilli> as Timer;
    uses interface Read<uint16_t> as Read;

	uses interface Process as SignalProcess;
    uses interface Process as RFProcess;


	uses interface RFMsgSend;

    //Radio TX
    //AMSenderC

	//ActiveMessageC
    uses interface SplitControl as AMControl;
    
    //Radio RX
    uses interface Receive;
}
implementation
{
	uint16_t temp_counter = 0;

    event void Boot.booted()
    {
		//This is not fully split-phase
		//The return value is supposed indicate whether the radio interface could be started or not
		//but the wrapper will always report success
        call AMControl.start();
    }

    event void AMControl.startDone(error_t err)
    {
        if (err != SUCCESS)
		{
			//This is not fully split-phase
			//The return value is supposed indicate whether the radio interface could be started or not
			//but the wrapper will always report success
            call AMControl.start();
            return;
		}
         
        call Timer.startPeriodic( TIMER_PERIOD_MILLI );
    }

    event void AMControl.stopDone(error_t err) 
    {
    }

    event void Timer.fired()
    {
		//This is not fully split-phase
		//The return value is supposed indicate whether the read could be started or not
		//but the wrapper will always report success
        call Read.read();
    }    
    
    event void Read.readDone( error_t result, uint16_t val )
    {
		if (result != SUCCESS)
			return;

		/*
		//This is not fully split-phase
		//The return value is supposed indicate whether the processing could be started or not
		//but the wrapper will always report success
		call SignalProcess.proc(val);
		*/
		//HACK - no signal processing for now, instead
		call RFMsgSend.SendRadioMsg( NODE_ID, val + temp_counter++ );
    }
    
	event void SignalProcess.procDone( error_t err, uint16_t val )
	{
		if (err != SUCCESS)
			return;

        call RFMsgSend.SendRadioMsg( NODE_ID, val );
	}

    event message_t* Receive.receive(message_t*	msg, 
									void*		payload, 
									uint8_t		len) 
    {
		RadioMsg* rpkt;

        if ( len != sizeof(RadioMsg) )
            return msg; //Not doing anything with this after return
    
        rpkt = (RadioMsg*) payload;
//		rpkt->nodeid 
//		rpkt->val

		//This is not fully split-phase
		//The return value is supposed indicate whether the processing could be started or not
		//but the wrapper will always report success
        call RFProcess.proc( rpkt->val );
        
        return msg; //Not doing anything with this after return
    }

	event void RFProcess.procDone( error_t err, uint16_t val )
	{
		if (err != SUCCESS)
			return;

        call RFMsgSend.SendRadioMsg( NODE_ID, val );
	}
   
}