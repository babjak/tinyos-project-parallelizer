#!/bin/bash

# Set up variables
SCRIPT="`readlink -e $0`"
SCRIPTPATH="`dirname $SCRIPT`"
parallelisator_dir=$SCRIPTPATH/../..
project_dir=$SCRIPTPATH/SenseForward_project_single_core
temp_dir=$SCRIPTPATH/temp

clear
echo "******************************************************************************"
echo "*"
echo "* Simulating multi-core project"
echo "*"
echo "******************************************************************************"
read -p "Press any key to start... " -n1 -s
echo "STARTED"

java -jar $parallelisator_dir/Avrora/avrora.jar -config-file=multi_core_config_3core.conf $temp_dir/RF.elf,$temp_dir/Main.elf,$temp_dir/Sense.elf $temp_dir/single_core.elf

echo "ALL DONE"


















