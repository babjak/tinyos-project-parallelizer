#!/bin/bash

# Set up variables
SCRIPT="`readlink -e $0`"
SCRIPTPATH="`dirname $SCRIPT`"
parallelisator_dir=$SCRIPTPATH/../..
project_dir=$SCRIPTPATH/SenseForward_project_single_core
temp_dir=$SCRIPTPATH/temp

mkdir -p $temp_dir
#read -p "Press any key to start... " -n1 -s

clear
echo "******************************************************************************"
echo "*"
echo "* 1. Building single-core project with XML dump output for subsequent analysis"
echo "*"
echo "******************************************************************************"
read -p "Press any key to start... " -n1 -s
echo "STARTED"

current_dir=$PWD
cd $project_dir
make -f $parallelisator_dir/Makefile_single_core micaz
cd $current_dir

cp $project_dir/build/micaz/wiring-check.xml $temp_dir
cp $project_dir/build/micaz/main.exe $temp_dir/single_core.elf

echo "ALL DONE"
read -p "Press any key to continue... " -n1 -s


clear
echo "******************************************************************************"
echo "*"
echo "* 2. Program topology analysis"
echo "*"
echo "******************************************************************************"
read -p "Press any key to start... " -n1 -s
echo "STARTED"

echo -n "Generating block diagram of single-core project... "
java -jar $parallelisator_dir/XSLT/saxon9he.jar -s:$temp_dir/wiring-check.xml -xsl:$parallelisator_dir/XSLT/Convert_To_GraphML.xsl -o:$temp_dir/graph_output.graphml
echo "DONE"

echo -n "Checking viability of flat partitioning... "
java -jar $parallelisator_dir/XSLT/saxon9he.jar -s:$temp_dir/wiring-check.xml -xsl:$parallelisator_dir/XSLT/Flat_Partitioning.xsl -o:$temp_dir/partitioning_guide_full_flat.xml input_param_file=$SCRIPTPATH/partitioning_guide_3core.xml 2> $temp_dir/flat_partitioning.log
echo "DONE"

echo -n "Checking viability of deep partitioning... "
java -jar $parallelisator_dir/XSLT/saxon9he.jar -s:$temp_dir/wiring-check.xml -xsl:$parallelisator_dir/XSLT/Deep_Partitioning.xsl -o:$temp_dir/Deep_Partitioning_output.xml input_param_file=$temp_dir/partitioning_guide_full_flat.xml 2> $temp_dir/deep_partitioning.log
echo "DONE"

echo -n "Generating seasoning... "
java -jar $parallelisator_dir/XSLT/saxon9he.jar -s:$temp_dir/wiring-check.xml -xsl:$parallelisator_dir/XSLT/Generate_Seasoning.xsl -o:$temp_dir/seasoning.xml input_param_file=$temp_dir/partitioning_guide_full_flat.xml 2> $temp_dir/seasoning.log
echo "DONE"


echo "ALL DONE"
read -p "Press any key to continue... " -n1 -s


clear
echo "******************************************************************************"
echo "*"
echo "* 3. Multi-core solution generation"
echo "*"
echo "******************************************************************************"
read -p "Press any key to start... " -n1 -s
echo "STARTED"

multi_core_solution_dir=$temp_dir/Multi_Core_solution

echo "Generating solution..."
java -classpath "$parallelisator_dir/FMPP/jaxen-1.1.1/jaxen-1.1.1.jar:$parallelisator_dir/FMPP/fmpp_0.9.14/lib/fmpp.jar" fmpp.tools.CommandLine -C $parallelisator_dir/Multi_Core_Solution.tmpl/data/config.fmpp -S $parallelisator_dir/Multi_Core_Solution.tmpl -O $multi_core_solution_dir --data-root=$temp_dir
#java -classpath "$parallelisator_dir/FMPP/jaxen-1.1.1/jaxen-1.1.1.jar:$parallelisator_dir/FMPP/fmpp_0.9.14/lib/fmpp.jar" fmpp.tools.CommandLine -C $parallelisator_dir/Multi_Core_Solution.tmpl/data/config.fmpp -S $parallelisator_dir/Multi_Core_Solution.tmpl -O $multi_core_solution_dir --data-root=$temp_dir -D "data: {seasoning: xml(seasoning.xml)}"
#cp $temp_dir/seasoning.xml $parallelisator_dir/Multi_Core_Solution.tmpl/data
#java -classpath "$parallelisator_dir/FMPP/jaxen-1.1.1/jaxen-1.1.1.jar:$parallelisator_dir/FMPP/fmpp_0.9.14/lib/fmpp.jar" fmpp.tools.CommandLine -C $parallelisator_dir/Multi_Core_Solution.tmpl/data/config.fmpp -S $parallelisator_dir/Multi_Core_Solution.tmpl -O $multi_core_solution_dir
echo "DONE"

echo "Copying original single-core solution files..."
cp $project_dir/*.nc $multi_core_solution_dir/Original
cp $project_dir/*.h $multi_core_solution_dir/Original
echo "DONE"
read -p "Press any key to continue... " -n1 -s


clear
echo "******************************************************************************"
echo "*"
echo "* 4. Building multi-core solution"
echo "*"
echo "******************************************************************************"
read -p "Press any key to start... " -n1 -s
echo "STARTED"

current_dir=$PWD
cd $multi_core_solution_dir
make multi_core_solution
cd $current_dir

mv $multi_core_solution_dir/*.elf $temp_dir

echo "ALL DONE"
