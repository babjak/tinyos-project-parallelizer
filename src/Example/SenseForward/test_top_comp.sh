#!/bin/bash

# Set up variables
SCRIPT="`readlink -e $0`"
SCRIPTPATH="`dirname $SCRIPT`"
parallelisator_dir=$SCRIPTPATH/../..
project_dir=$SCRIPTPATH/SenseForward_project_single_core
temp_dir=$SCRIPTPATH/temp


java -jar $parallelisator_dir/XSLT/saxon9he.jar -s:$temp_dir/wiring-check.xml -xsl:$parallelisator_dir/XSLT/Find_Top_Comp.xsl -o:$temp_dir/top_comp.xml input_param_file=$project_dir/Input_Params.xml
