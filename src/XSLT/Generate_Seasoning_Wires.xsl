<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Helper_Funcs.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="no" encoding='UTF-8' />


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>

	<xsl:variable	name="top_comp"		select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/top_comp/comp[1]/@name]"/>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template name="bn:Generate_Seasoning_Wires">

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#009;</xsl:text>
		<wires xmlns="">

		<xsl:variable	name="wires"	select="$top_comp/n:wiring/n:wire"/>
<!--
		<xsl:variable	name="all_wires"	select="$top_comp/n:wiring/n:wire"/>
		<xsl:variable	name="comps"		select="$doc_root/n:nesc/n:components/n:component[@qname = current-group()/@name]"/>

		<xsl:variable	name="wires"		select="$all_wires[not(bn:Is_Inter_Core_Wire(., document($input_param_file)/params/comp_list/comp))]"/>
-->
		<xsl:for-each select="$wires">

			<xsl:text>&#xA;</xsl:text>
			<xsl:text>&#009;</xsl:text>
			<xsl:text>&#009;</xsl:text>
			<xsl:element name="wire" namespace="">


				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:element name="from" namespace="">

					<xsl:element name="interface_ref" namespace="">
						<xsl:attribute name="name_hint"><xsl:value-of select="./n:from/n:interface-ref/@name"/></xsl:attribute>
						<xsl:attribute name="ref"><xsl:value-of select="./n:from/n:interface-ref/@ref"/></xsl:attribute>
					</xsl:element>

				</xsl:element>


				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:element name="to" namespace="">

					<xsl:element name="interface_ref" namespace="">
						<xsl:attribute name="name_hint"><xsl:value-of select="./n:to/n:interface-ref/@name"/></xsl:attribute>
						<xsl:attribute name="ref"><xsl:value-of select="./n:to/n:interface-ref/@ref"/></xsl:attribute>
					</xsl:element>

				</xsl:element>


				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
			</xsl:element>

		</xsl:for-each>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#009;</xsl:text>
		</wires>

	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

		<xsl:call-template name="bn:Generate_Seasoning_Wires">
<!--
			<xsl:with-param name ="core_IDs_param"			select ="$rest_core_IDs"></xsl:with-param>
-->
		</xsl:call-template>

	</xsl:template>

</xsl:stylesheet>

