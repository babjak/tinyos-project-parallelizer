<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Helper_Funcs.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="no" encoding='UTF-8' />


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>
<!--
	<xsl:variable	name="input_param_file"			select="'Input_Params.xml'"/>
-->

	<xsl:variable	name="dedicated_comps"			select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/dedicated_comp_list/comp/@name]"/>
	<xsl:variable	name="copiable_comps"			select="/n:nesc/n:components/n:component[(@qname = document($input_param_file)/params/copiable_comp_list/comp/@name)
																							and
																							not(. = dedicated_comps)]"/>
	<xsl:variable	name="top_comp"					select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/top_comp/comp[1]/@name]"/>
	<xsl:variable	name="cuttable_interface_names"	select="document($input_param_file)/params/cuttable_interface_list/interface/@name"/>
	<xsl:variable	name="wires"					select="$top_comp/n:wiring/n:wire"/>
	<xsl:variable	name="doc_root"					select="/"/>


	<xsl:variable	name="empty_set"				select="()"/>

	<xsl:variable	name="tainted_state"			select="'tainted'"/>
	<xsl:variable	name="normal_state"				select="'normal'"/>

<!--
	<xsl:key name="components_by_name"                  match="n:component"             use="@qname"/>
	<xsl:key name="components_by_wire_from"				match="n:component"             use="n:wiring/n:wire/n:from/n:interface-ref/@ref"/>
	<xsl:key name="components_by_wire_to"				match="n:component"             use="n:wiring/n:wire/n:to/n:interface-ref/@ref"/>
	<xsl:key name="wires_by_from_interface_ref"         match="n:nesc/n:wiring/n:wire"  use="n:from/n:interface-ref/@ref"/>		
	<xsl:key name="wires_by_to_interface_ref"           match="n:nesc/n:wiring/n:wire"  use="n:to/n:interface-ref/@ref"/>		
	<xsl:key name="interfaces_by_component_ref_qname"   match="n:interface"             use="n:component-ref/@qname"/>		
	<xsl:key name="interfaces_by_ref"                   match="n:interface"             use="@ref"/>		
-->

<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Find_Chain_Rec">

		<xsl:param		name="comps_param"		/> <!-- List of components to be checked -->
		<xsl:param		name="start_comp_param"	/> <!-- The previous component -->
		<xsl:param		name="wires_param"/>


		<xsl:variable	name="comps"				select="$comps_param[not(@qname = $start_comp_param/@qname)]"/>
		<xsl:variable	name="relevant_ifs"			select="bn:Get_Connecting_Interfaces($start_comp_param, $wires_param)"/>
		<xsl:variable	name="start_ifs"			select="$relevant_ifs[n:component-ref/@qname = $start_comp_param/@qname]"/>		
		<xsl:variable	name="no_cut_ifs"			select="$start_ifs[not(n:instance/n:interfacedef-ref/@qname = $cuttable_interface_names)]"/>		
		<xsl:variable	name="con_ifs"				select="$relevant_ifs[not(@ref = $start_ifs/@nref) 
																		and 
																		(n:component-ref/@qname = $comps/@qname)]"/>


		<xsl:choose>
			<xsl:when test="not( $con_ifs )">

				<xsl:sequence	select="$start_comp_param/@qname"/>

			</xsl:when>

			<xsl:otherwise>

				<xsl:variable	name="comp_wire"			select="$wires_param[(
																					(n:from/n:interface-ref/@ref = $no_cut_ifs/@ref) 
																					and 
																					(n:to/n:interface-ref/@ref = $con_ifs/@ref)
																				)
																				or
																				(
																					(n:from/n:interface-ref/@ref = $con_ifs/@ref) 
																					and
																					(n:to/n:interface-ref/@ref = $no_cut_ifs/@ref)
																				)][1]"/>
				<xsl:variable	name="from_if"				select="$doc_root/n:nesc/n:interfaces/n:interface[ @ref = $comp_wire/n:from/n:interface-ref/@ref ]"/>
				<xsl:variable	name="to_if"				select="$doc_root/n:nesc/n:interfaces/n:interface[ @ref = $comp_wire/n:to/n:interface-ref/@ref ]"/>
				<xsl:variable	name="start_if"				select="$no_cut_ifs[(@ref = $from_if/@ref) or (@ref = $to_if/@ref)][1]"/>
				<xsl:variable	name="stop_if"				select="$con_ifs[(@ref = $from_if/@ref) or (@ref = $to_if/@ref)][1]"/>

				<xsl:variable	name="con_comp"				select="$doc_root/n:nesc/n:components/n:component[ @qname = $stop_if/n:component-ref/@qname ]"/>

				<xsl:sequence	select="concat('Component: ',		$start_comp_param/@qname, 
												', Interface:',		$start_if/@name, 
												', Type:',			$start_if/n:instance/n:interfacedef-ref/@qname,
												', Interface:',		$stop_if/@name,
												', ', bn:Find_Chain_Rec($comps, $con_comp, $wires_param))"/>

			</xsl:otherwise>
		</xsl:choose>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Is_Cuttable_Connection">

		<xsl:param		name="component1_param"/>
		<xsl:param		name="component2_param"/>
		<xsl:param		name="wires_param"/>


		<xsl:choose>
			<xsl:when test="not($component1_param) or not($component2_param)">

				<xsl:sequence	select="false()"/>

			</xsl:when>

			<xsl:otherwise>

				<!-- Get all the interfaces for component -->
				<xsl:variable	name="comp1_interfaces"		select="$doc_root/n:nesc/n:interfaces/n:interface[n:component-ref/@qname = $component1_param[1]/@qname]"/>
				<xsl:variable	name="comp2_interfaces"		select="$doc_root/n:nesc/n:interfaces/n:interface[n:component-ref/@qname = $component2_param[1]/@qname]"/>

				<!-- Out of the specified wires get those that have the above interfaces on both ends -->
				<xsl:variable	name="comp_wires"			select="$wires_param[(
																					(n:from/n:interface-ref/@ref = $comp1_interfaces/@ref) 
																					and 
																					(n:to/n:interface-ref/@ref = $comp2_interfaces/@ref)
																				)
																				or
																				(
																					(n:from/n:interface-ref/@ref = $comp2_interfaces/@ref) 
																					and
																					(n:to/n:interface-ref/@ref = $comp1_interfaces/@ref)
																				)]"/>

				<!-- Get interfaces that connect to the above specified wires -->
				<xsl:variable	name="relevant_interfaces"	select="$doc_root/n:nesc/n:interfaces/n:interface[ (@ref = $comp_wires/n:from/n:interface-ref/@ref) or (@ref = $comp_wires/n:to/n:interface-ref/@ref) ]"/>

				<!-- Any of these interfaces not cuttable? -->
				<xsl:variable	name="not_cuttable_ifs"		select="$relevant_interfaces[ not(n:instance/n:interfacedef-ref/@qname = $cuttable_interface_names) ]"/>

				<xsl:sequence	select="not( $not_cuttable_ifs )"/>

			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Horizontal_Search_Rec">

		<xsl:param		name="components_param"		/> <!-- List of components to be checked -->
		<xsl:param		name="prev_component_param"	/> <!-- The previous component -->
		<xsl:param		name="potential_comps_param"/> <!-- List of components that checked out OK so far -->
		<xsl:param		name="assigned_comps_param"/> <!-- Components already assigned to other cores -->
		<xsl:param		name="find_shortest_path_param"/> <!-- mode selection -->
		<xsl:param		name="indent_param"/> <!--  -->

		<!-- Take only components we don't already have as potential candidates -->
		<xsl:variable	name="components"	select="$components_param[ not(@qname = $potential_comps_param/@qname) ]"/>
		<xsl:variable	name="comps_empty"	select="not( boolean( $components ) )"/>

<!--														   
		<xsl:message><xsl:value-of select="$indent_param"/>1111111111111111111111111111111111111111111111111111111111111111111111111</xsl:message>			
		<xsl:message><xsl:value-of select="$indent_param"/>Components to check:         <xsl:value-of select="$components/@qname" separator=", "/></xsl:message>
-->

		<xsl:choose>
			<xsl:when test="$comps_empty">

<!--
				<xsl:message><xsl:value-of select="$indent_param"/>Empty!</xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>555555555555555555555555555555555555555555555555555555555555555555555555</xsl:message>			
-->

				<!-- Nothing to do -->
				<xsl:sequence	select="$normal_state"/>
			</xsl:when>

			<xsl:otherwise>

				<!-- Check first component -->
				<xsl:variable	name="component"			select="$components[1]"/>
				<xsl:variable	name="rest_comps"			select="$components[position() > 1]"/>
				<xsl:variable	name="comp_assigned"		select="boolean( $assigned_comps_param[@qname = $component/@qname] )"/>
				<xsl:variable	name="cuttable_wire"		select="boolean( bn:Is_Cuttable_Connection($component, $prev_component_param, $wires) )"/>
				<xsl:variable	name="comp_copiable"		select="boolean( $copiable_comps[@qname = $component/@qname] )"/>
				<xsl:variable	name="con_comps"			select="if ( not( $comp_assigned ) ) then
																		bn:Get_Connecting_Components($component, $wires)[not(@qname = ($potential_comps_param | $component)/@qname)]
																	else
																		()"/>

<!--
				<xsl:message><xsl:value-of select="$indent_param"/>component:     <xsl:value-of select="$component/@qname"/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>rest_comps:    <xsl:value-of select="$rest_comps/@qname" separator=", "/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>comp_assigned: <xsl:value-of select="$comp_assigned"/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>cuttable_wire: <xsl:value-of select="$cuttable_wire"/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>comp_copiable: <xsl:value-of select="$comp_copiable"/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>con_comps:     <xsl:value-of select="$con_comps/@qname" separator=", "/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>2222222222222222222222222222222222222222222222222222222222222222222222222</xsl:message>
-->

				<!-- ######################################################################################### -->
				<!-- Depth search -->
				
				<xsl:variable	name="result"				select="if ( not($comp_assigned) and $con_comps ) then
																		bn:Horizontal_Search_Rec($con_comps,
																								$component,
																								$potential_comps_param | $component,
																								$assigned_comps_param,
																								$find_shortest_path_param,
																								concat($indent_param, '  '))
																	else
																		($normal_state)"/>

																		
				<!-- If any of the connecting components is tainted, this result shows tainted --> 
				<xsl:variable	name="ret_state"			select="if ( $cuttable_wire or $comp_copiable ) then
																		$normal_state
																	else if ( $comp_assigned ) then
																		$tainted_state
																	else
																		$result[1]" /> 

				<xsl:variable	name="ret_comp_list"		select="if ( $find_shortest_path_param ) then
																		if ( ($result[1] = $tainted_state) and not($comp_copiable) and not($cuttable_wire) and not( $comp_assigned ) ) then
																			$component | $result[position() > 1]
																		else if ( not($cuttable_wire) and $comp_assigned ) then
																			$component
																		else
																			()
																	else
																		if ( not($comp_assigned) and $result[1] = $normal_state ) then
																			($component | $result[position() > 1])
																		else if ( $comp_copiable ) then
																			($component)
																		else
																			()"/>																			
																			
<!--
				<xsl:message><xsl:value-of select="$indent_param"/>Depth search ret_state:      <xsl:value-of select="$ret_state"/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>Depth search ret_comp_list:  <xsl:value-of select="$ret_comp_list/@qname" separator=", "/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>3333333333333333333333333333333333333333333333333333333333333333333333333</xsl:message>
-->																			

				<!-- ######################################################################################### -->
				<!-- Recursive horizontal search -->
				<xsl:variable	name="result"				select="if ( $rest_comps ) then
																		bn:Horizontal_Search_Rec($rest_comps,
																								$prev_component_param,
																								$potential_comps_param | $ret_comp_list,
																								$assigned_comps_param,
																								$find_shortest_path_param,
																								concat($indent_param, '  '))
																	else
																		$normal_state"/>

				<xsl:variable	name="ret_state2"			select="$result[1]" />
				<xsl:variable	name="ret_comp_list2"		select="$result[position() > 1]" />


<!--
				<xsl:if test="$rest_comps">
					<xsl:message><xsl:value-of select="$indent_param"/>Horiz search ret_state2:     <xsl:value-of select="$ret_state2"/></xsl:message>
					<xsl:message><xsl:value-of select="$indent_param"/>Horiz search ret_comp_list2: <xsl:value-of select="$ret_comp_list2/@qname" separator=", "/></xsl:message>
					<xsl:message><xsl:value-of select="$indent_param"/>4444444444444444444444444444444444444444444444444444444444444444444444444</xsl:message>
				</xsl:if>																		
-->

				<!-- ######################################################################################### -->
				<!-- Generate return value -->
				<xsl:variable	name="state"				select="if (($ret_state = $tainted_state) or ($ret_state2 = $tainted_state)) then
																		$tainted_state
																	else
																		$normal_state" />

				<xsl:variable	name="comp_list"			select="if ( $find_shortest_path_param ) then
																		if ( (count($ret_comp_list2) &gt; 0) and (count($ret_comp_list) = 0) ) then 
																			$ret_comp_list2
																		else if ( (count($ret_comp_list) &gt; 0) and (count($ret_comp_list2) = 0) ) then 
																			$ret_comp_list
																		else if ( (count($ret_comp_list) = 0) and (count($ret_comp_list2) = 0) ) then 
																			()
																		else if ( count($ret_comp_list2) &lt; count($ret_comp_list) ) then
																			$ret_comp_list2
																		else
																			$ret_comp_list
																	else
																		$ret_comp_list | $ret_comp_list2" />

<!--
				<xsl:message><xsl:value-of select="$indent_param"/>state:                       <xsl:value-of select="$state"/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>comp_list:                   <xsl:value-of select="$comp_list/@qname" separator=", "/></xsl:message>
				<xsl:message><xsl:value-of select="$indent_param"/>555555555555555555555555555555555555555555555555555555555555555555555555</xsl:message>
-->
				<xsl:sequence	select="$state , $comp_list"/>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template name="bn:Flat_Partitioning_Rec">

		<xsl:param		name="core_IDs_param"/> <!--  -->
		<xsl:param		name="partitioned_comps_param"/>

		<xsl:variable	name="core_IDs"			select="distinct-values($core_IDs_param)"/>
		<xsl:variable	name="core_IDs_empty"	select="empty( $core_IDs )"/>

		<xsl:choose>
			<xsl:when test="$core_IDs_empty">
				<!-- Nothing to do -->

			</xsl:when>

			<xsl:otherwise>

				<xsl:variable	name="core_ID"			select="$core_IDs[1]"/>
				<xsl:variable	name="rest_core_IDs"	select="$core_IDs[position() > 1]"/>


				<xsl:variable	name="comps"	select="$doc_root/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/dedicated_comp_list/comp[@core_ID = $core_ID]/@name]"/>

				<xsl:variable	name="result"	select="bn:Horizontal_Search_Rec( $comps, 
																				$empty_set, 
																				$empty_set, 
																				($dedicated_comps[ not(@qname = $comps/@qname) ], $partitioned_comps_param),
																				false(),
																				'   ')"/>
				<xsl:for-each select="$result[position() > 1]/@qname">
					<xsl:element name="comp" namespace="">
						<xsl:attribute name="name"><xsl:value-of select="."/></xsl:attribute>
						<xsl:attribute name="core_ID"><xsl:value-of select="$core_ID"/></xsl:attribute>
					</xsl:element>
					<xsl:text>&#xA;</xsl:text>

				</xsl:for-each>

				<xsl:if test="$result[1] = $tainted_state">
					<xsl:variable	name="shortest_path"	select="bn:Horizontal_Search_Rec( $comps, 
																							$empty_set, 
																							$empty_set, 
																							($dedicated_comps[ not(@qname = $comps/@qname) ], $partitioned_comps_param),
																							true(),
																							'   ')[position() > 1]"/>
					<xsl:element name="shortest_error_path"><xsl:value-of	select="$shortest_path/@qname" separator=", "/></xsl:element>
					<xsl:text>&#xA;</xsl:text>
					<xsl:element name="detailed_error_path"><xsl:value-of	select="bn:Find_Chain_Rec($shortest_path, $comps[@qname = $shortest_path/@qname][1], $wires)"/></xsl:element>
					<xsl:text>&#xA;</xsl:text>
				</xsl:if>

				<xsl:text>&#xA;</xsl:text>

				<xsl:call-template name="bn:Flat_Partitioning_Rec">
					<xsl:with-param name ="core_IDs_param"			select ="$rest_core_IDs"></xsl:with-param>
					<xsl:with-param name ="partitioned_comps_param"	select ="($partitioned_comps_param, $result[position() > 1])"></xsl:with-param>
				</xsl:call-template>

			</xsl:otherwise>

		</xsl:choose>

	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

		<xsl:text>&#xA;</xsl:text>
		<params xmlns="">
		<xsl:text>&#xA;</xsl:text><xsl:text>&#xA;</xsl:text>


			<xsl:copy-of copy-namespaces="no" select="document($input_param_file)/params/top_comp"/>
			<xsl:text>&#xA;</xsl:text><xsl:text>&#xA;</xsl:text>
			<xsl:copy-of copy-namespaces="no" select="document($input_param_file)/params/core_list"/>
			<xsl:text>&#xA;</xsl:text><xsl:text>&#xA;</xsl:text>
			<xsl:copy-of copy-namespaces="no" select="document($input_param_file)/params/dedicated_comp_list"/>
			<xsl:text>&#xA;</xsl:text><xsl:text>&#xA;</xsl:text>
			<xsl:copy-of copy-namespaces="no" select="document($input_param_file)/params/cuttable_interface_list"/>
			<xsl:text>&#xA;</xsl:text><xsl:text>&#xA;</xsl:text>
			<xsl:copy-of copy-namespaces="no" select="document($input_param_file)/params/copiable_comp_list"/>
			<xsl:text>&#xA;</xsl:text><xsl:text>&#xA;</xsl:text>

			<comp_list>
			<xsl:text>&#xA;</xsl:text>

			<xsl:call-template name="bn:Flat_Partitioning_Rec">
				<xsl:with-param name ="core_IDs_param"			select ="document($input_param_file)/params/dedicated_comp_list/comp/@core_ID"></xsl:with-param>
				<xsl:with-param name ="partitioned_comps_param"	select ="()"></xsl:with-param>
			</xsl:call-template>

			</comp_list>

		<xsl:text>&#xA;</xsl:text>
		</params>

	</xsl:template>

</xsl:stylesheet>

