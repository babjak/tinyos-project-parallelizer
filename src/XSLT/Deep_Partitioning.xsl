<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0"
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform"
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Helper_Funcs.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="yes" encoding='UTF-8' />


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>
<!--
	<xsl:variable	name="input_param_file"			select="'Input_Params.xml'"/>
-->
	
	<xsl:variable	name="dedicated_comps"			select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/comp_list/comp/@name]"/>
	<xsl:variable	name="copiable_comps"			select="/n:nesc/n:components/n:component[(@qname = document($input_param_file)/params/copiable_comp_list/comp/@name)
																							and
																							not(. = dedicated_comps)]"/>
	<xsl:variable	name="top_comp"					select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/top_comp/comp[1]/@name]"/>
	<xsl:variable	name="cuttable_interface_names"	select="document($input_param_file)/params/cuttable_interface_list/interface/@name"/>
	<xsl:variable	name="wires"					select="$top_comp/n:wiring/n:wire"/>
	<xsl:variable	name="doc_root"					select="/"/>

	<xsl:variable	name="empty_set"				select="()"/>



<!--	
	<xsl:key name="components_by_name"                  match="n:component"             use="@qname"/>
	<xsl:key name="components_by_wire_from"				match="n:component"             use="n:wiring/n:wire/n:from/n:interface-ref/@ref"/>
	<xsl:key name="components_by_wire_to"				match="n:component"             use="n:wiring/n:wire/n:to/n:interface-ref/@ref"/>
	<xsl:key name="wires_by_from_interface_ref"         match="n:nesc/n:wiring/n:wire"  use="n:from/n:interface-ref/@ref"/>		
	<xsl:key name="wires_by_to_interface_ref"           match="n:nesc/n:wiring/n:wire"  use="n:to/n:interface-ref/@ref"/>		
	<xsl:key name="interfaces_by_component_ref_qname"   match="n:interface"             use="n:component-ref/@qname"/>		
	<xsl:key name="interfaces_by_ref"                   match="n:interface"             use="@ref"/>		
-->

<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	

	
	
	<xsl:variable	name="s_tainted_state"			select="'tainted'"/>
	<xsl:variable	name="s_normal_state"			select="'normal'"/>
	
<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Search_Rec">

		<xsl:param		name="components_param"		/> <!-- List of components to be checked -->
		<xsl:param		name="ignore_comps_param"/> <!-- Components to be ignored for analysis -->
		<xsl:param		name="raise_flag_comps_param"/> <!--  -->
		<xsl:param		name="current_path_param"/> <!--  -->
		<xsl:param		name="indent_param"/> <!--  -->

		
		<xsl:variable	name="components"	select="$components_param[ not(@qname = $ignore_comps_param/@qname) and not(@qname = $current_path_param/@qname) ]"/>
		<xsl:variable	name="comps_empty"	select="not( boolean( $components ) )"/>		
		
		
		<xsl:choose>
			<xsl:when test="$comps_empty">
			
				<!-- Nothing to do -->
				<xsl:sequence	select="$s_normal_state"/>
			</xsl:when>
			
			<xsl:otherwise>
		
				<!-- Check first component -->
				<xsl:variable	name="component"			select="$components[1]"/>
				<xsl:variable	name="rest_comps"			select="$components[position() > 1]"/>
				<xsl:variable	name="comp_flag"			select="boolean( $raise_flag_comps_param[@qname = $component/@qname] )"/>
				
				<!-- ######################################################################################### -->
				<!-- Depth search -->
				
				<xsl:variable	name="result"				select="if ( not($comp_flag) ) then
																		bn:Search_Rec(	bn:Get_Contained_Components($component),
																						$ignore_comps_param,
																						$raise_flag_comps_param,
																						$current_path_param | $component,
																						concat($indent_param, ' '))
																	else
																		()"/>

				<xsl:variable	name="ret_state"			select="if ( $comp_flag ) then
																		$s_tainted_state
																	else
																		$result[1]" />

				<xsl:variable	name="ret_comp_list"		select="( $result[position() > 1], $component )" />

<!--																		
				<xsl:variable	name="ret_comp_list"		select="if ( $comp_flag ) then
																		$component
																	else if ( $result[1] = $s_tainted_state ) then
																		$result[position() > 1] 
																	else
																		$component | $result[position() > 1]" />																						
-->																		
				
				<!-- ######################################################################################### -->
				<!-- Recursive horizontal search -->

				<xsl:variable	name="result"				select="if ($ret_state = $s_normal_state) then
																		bn:Search_Rec(	$rest_comps,
																						$ignore_comps_param,
																						$raise_flag_comps_param,
																						$current_path_param,
																						$indent_param)
																	else
																		()"/>
																						
				<xsl:variable	name="ret_state2"			select="$result[1]" />
				<xsl:variable	name="ret_comp_list2"		select="$result[position() > 1]" />	
					

				<!-- ######################################################################################### -->
				<!-- Generate return value -->
																		
				<xsl:variable	name="state"				select="if (($ret_state = $s_tainted_state) or ($ret_state2 = $s_tainted_state)) then
																		$s_tainted_state
																	else
																		$s_normal_state" />
																		
				<xsl:variable	name="comp_list"			select="if ($ret_state = $s_tainted_state) then 
																		$ret_comp_list
																	else if ($ret_state2 = $s_tainted_state) then 
																		$ret_comp_list2
																	else
																		($ret_comp_list, $ret_comp_list2)" />

																		
				<xsl:sequence	select="($state, $comp_list)"/>
					
			</xsl:otherwise>
		</xsl:choose>
	
	</xsl:function>

	
<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Find_Shared_Component_Rec">

		<xsl:param		name="sequential_number_param"/> <!-- List of components to be checked -->
		<xsl:param		name="raise_flag_comps_param" />
		
		
		<xsl:variable	name="core_IDs"				select="distinct-values( document($input_param_file)/params/comp_list/comp/@core_ID )" />
		<xsl:variable	name="core_ID"				select="$core_IDs[$sequential_number_param]" />
		<xsl:variable	name="comp_names"			select="distinct-values( document($input_param_file)/params/comp_list/comp[@core_ID = $core_ID]/@name )" />
		<xsl:variable	name="comps"				select="$doc_root/n:nesc/n:components/n:component[@qname =  $comp_names]" />
		<xsl:variable	name="comps_empty"			select="not( boolean( $comps ) )"/>
		
		<xsl:choose>		
			<xsl:when test="$comps_empty">
			
				<!-- Nothing to do -->
				<xsl:sequence	select="$s_normal_state"/>
			</xsl:when>
			
			<xsl:otherwise>
			
				<!-- ######################################################################################### -->
				<!-- Check hierarchical components for given core. (Recursively collect all contained components)  -->				
			
				<xsl:variable	name="ignore_comp_names"	select="distinct-values( document($input_param_file)/params/copiable_comp_list/comp/@name )"/>
				<xsl:variable	name="ignore_comps"			select="$doc_root/n:nesc/n:components/n:component[@qname =  $ignore_comp_names]" />

			
				<xsl:variable	name="result"				select="bn:Search_Rec(	$comps, 
																					$ignore_comps,
																					$raise_flag_comps_param,
																					(),
																					'')"/>
																		
				<xsl:variable	name="ret_state"			select="$result[1]" />
				<xsl:variable	name="ret_comp_list"		select="$result[position() > 1]" />

				
				<!-- ######################################################################################### -->
				<!-- Check hierarchical components for the other cores (if needed) -->				

				<!-- If a single component is found that was dedicated for a previous core, we'll stop.  -->				
				<xsl:variable	name="flag_raised"			select="$ret_state = $s_tainted_state" />
				
				<xsl:variable	name="result"				select="if ($flag_raised) then
																		()
																	else
																		bn:Find_Shared_Component_Rec(	$sequential_number_param + 1, 
																										$raise_flag_comps_param | $ret_comp_list )"/>

				<xsl:variable	name="ret_state2"			select="$result[1]" />
				<xsl:variable	name="ret_comp_list2"		select="$result[position() > 1]" />

				
				<!-- ######################################################################################### -->
				<!-- Generate return value -->
				
				<xsl:variable	name="state"				select="if (($ret_state = $s_tainted_state) or ($ret_state2 = $s_tainted_state)) then
																		$s_tainted_state
																	else
																		$s_normal_state" />
																		
				<xsl:variable	name="comp_list"			select="if ($ret_state = $s_tainted_state) then 
																		$ret_comp_list
																	else if ($ret_state2 = $s_tainted_state) then 
																		$ret_comp_list2
																	else
																		()" />
																		
				<xsl:sequence	select="($state, $comp_list)"/>																		
				
			</xsl:otherwise>
		</xsl:choose>
	
	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

			<xsl:variable	name="result"				select="bn:Find_Shared_Component_Rec( 1, () )"/>

			<xsl:variable	name="ret_state"			select="$result[1]" />
			<xsl:variable	name="ret_comp"				select="$result[2]" />

			<xsl:if			test="$ret_state = $s_tainted_state">
Error with component:
	<xsl:value-of select="$ret_comp/@qname"/>
			</xsl:if>

			<xsl:for-each-group select="document($input_param_file)/params/comp_list/comp" group-by="@core_ID">

For <xsl:value-of select="current-group()[1]/@core_ID"/> aka &quot;<xsl:value-of select="document($input_param_file)/params/core_list/core[@ID = current-group()[1]/@core_ID]/@description"/>&quot;:

				<xsl:variable	name="comps"	select="$doc_root/n:nesc/n:components/n:component[@qname = current-group()/@name]"/>
	Dedicated components:
		<xsl:value-of	select="$comps/@qname" separator=",&#xA;&#009;&#009;"/>			

				<xsl:choose>		
					<xsl:when test="$ret_state = $s_tainted_state">
					
						<xsl:variable	name="ignore_comp_names"	select="distinct-values( document($input_param_file)/params/copiable_comp_list/comp/@name )"/>
						<xsl:variable	name="ignore_comps"			select="$doc_root/n:nesc/n:components/n:component[@qname =  $ignore_comp_names]" />

						<xsl:variable	name="result"	select="bn:Search_Rec(	$comps, $ignore_comps, $ret_comp, (), '   ' )"/>
						
	Shortest error path:
		<xsl:value-of	select="if ($result[1] = $s_tainted_state) then
									$result[position() > 1]/@qname
								else
									()" separator=",&#xA;&#009;&#009;"/>
				
					</xsl:when>
			
					<xsl:otherwise>
					
						<xsl:variable	name="result"	select="bn:Search_Rec(	$comps, (), (), (), '   ' )"/>
						
	Partitioning result:
		<xsl:value-of	select="$result[1]"/>
	Partitioned comps:
		<xsl:value-of	select="$result[position() > 1]/@qname" separator=",&#xA;&#009;&#009;"/>
																			
					</xsl:otherwise>
				</xsl:choose>
							
			</xsl:for-each-group>
	</xsl:template>

</xsl:stylesheet>

