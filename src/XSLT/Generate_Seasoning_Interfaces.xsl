<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Helper_Funcs.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="no" encoding='UTF-8' />


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>

	<xsl:variable	name="top_comp"		select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/top_comp/comp[1]/@name]"/>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template name="bn:Funcion_Handle_Stuff">
		<xsl:param name ="val_param"/>

		<xsl:variable name="ret_type" select="if (./n:type-function/n:type-pointer) then
													concat(./n:type-function/n:type-pointer/*[starts-with(string(node-name(.)),'type-')]/n:typename/n:typedef-ref[1]/@name, '*')
												else
													./n:type-function/*[starts-with(string(node-name(.)),'type-')]/n:typename/n:typedef-ref[1]/@name"/>

		<xsl:variable name="func_params" select="./n:parameters/n:variable"/> 


		<xsl:if test="$ret_type or $func_params">

			<xsl:if test="$func_params">
				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:element name="func_params" namespace="">

					<xsl:for-each select="$func_params">	
													
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:element name="param" namespace="">
							<xsl:attribute name="type"><xsl:value-of select="if (./n:type-pointer) then
																				if (./n:type-pointer/*/n:typename) then
																					concat(./n:type-pointer/*/n:typename/n:typedef-ref/@name,'*')
																				else if (./n:type-pointer/n:type-void) then
																					'void*'
																				else
																					'UNKNOWN POINTER TYPE'
																			else if (./*/n:typename) then
																				./*/n:typename/n:typedef-ref/@name
																			else if (./*/n:typedef-ref) then
																				$val_param
																			else 
																				'UNKNOWN TYPE'"/></xsl:attribute>
							<!--
							<xsl:attribute name="true_type"><xsl:value-of select="./*/@cname"/></xsl:attribute>
							-->
							<xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>
						</xsl:element>
					</xsl:for-each>

					<xsl:text>&#xA;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
				</xsl:element>

			</xsl:if>


			<xsl:variable name="default_val" select="if ($ret_type = 'error_t') then
														'SUCCESS' 
													else 
														'0'"/>
			<xsl:if test="$ret_type">
				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:element name="return" namespace="">
					<xsl:attribute name="type"><xsl:value-of select="$ret_type"/></xsl:attribute>
					<xsl:attribute name="default_val"><xsl:value-of select="$default_val"/></xsl:attribute>
											
				</xsl:element>

			</xsl:if>

		</xsl:if>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->

	<xsl:template name="bn:Generate_Seasoning_Interfaces">

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#009;</xsl:text>
		<interfaces xmlns="">

			<xsl:variable name="wires"		select="$top_comp/n:wiring/n:wire"/>

			<xsl:variable name="inter_core_wires"			select="$wires[bn:Is_Inter_Core_Wire(., document($input_param_file)/params/comp_list/comp)]"/>

			<xsl:for-each select="$inter_core_wires">
				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>

				<xsl:variable name="from_interface"	select="$doc_root/n:nesc/n:interfaces/n:interface[ (@ref = current()/n:from/n:interface-ref/@ref) ]"/>

				<xsl:variable name="to_interface"	select="$doc_root/n:nesc/n:interfaces/n:interface[ (@ref = current()/n:to/n:interface-ref/@ref) ]"/>

				<xsl:variable name="interf_real_name" 	select="concat($from_interface/n:instance/n:interfacedef-ref/@qname, if ($from_interface/n:instance/n:arguments) then
																	concat('&lt;', $from_interface/n:instance/n:arguments/n:*/n:typename/n:typedef-ref/@name, '&gt;')
																else
																	())"/>

				<xsl:element name="interface" namespace="">
					<xsl:attribute name="name_from"><xsl:value-of select="$from_interface/@name"/></xsl:attribute>
					<xsl:attribute name="name_to"><xsl:value-of select="$to_interface/@name"/></xsl:attribute>
					<xsl:if test="$interf_real_name and (not($from_interface/@name = $interf_real_name) or not($to_interface/@name = $interf_real_name))">
						<xsl:attribute name="real_name"><xsl:value-of select="$interf_real_name"/></xsl:attribute>
					</xsl:if>
					<xsl:attribute name="from_ref"><xsl:value-of select="./n:from/n:interface-ref/@ref"/></xsl:attribute>
					<xsl:attribute name="to_ref"><xsl:value-of select="./n:to/n:interface-ref/@ref"/></xsl:attribute>

	<!--
					<xsl:variable	name="function_names"	select="./n:interface-functions/n:function-ref/@name"/>
	-->
					<xsl:variable	name="commands"	select="$doc_root/n:nesc/n:interfacedefs/n:interfacedef[@qname = $from_interface/n:instance/n:interfacedef-ref/@qname]/n:function[ @command ]"/>

					<xsl:if test="$commands">
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<commands xmlns="">
							<xsl:for-each select="$commands">
								<xsl:text>&#xA;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:element name="command" namespace="">
									<xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>

									<xsl:call-template name="bn:Funcion_Handle_Stuff">
										<xsl:with-param name ="val_param" select ="$from_interface/n:instance/n:arguments/*[1]/n:typename/n:typedef-ref/@name"/>
									</xsl:call-template>

									<xsl:text>&#xA;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
								</xsl:element>

							</xsl:for-each>

							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
						</commands>
					</xsl:if>

					<xsl:variable	name="events"	select="$doc_root/n:nesc/n:interfacedefs/n:interfacedef[@qname = $from_interface/n:instance/n:interfacedef-ref/@qname]/n:function[ @event ]"/>

					<xsl:if test="$events">
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<events xmlns="">
							<xsl:for-each select="$events">
								<xsl:text>&#xA;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:element name="event" namespace="">
									<xsl:attribute name="name"><xsl:value-of select="./@name"/></xsl:attribute>

									<xsl:call-template name="bn:Funcion_Handle_Stuff">
										<xsl:with-param name ="val_param" select ="$from_interface/n:instance/n:arguments/*[1]/n:typename/n:typedef-ref/@name"/>
									</xsl:call-template>

									<xsl:text>&#xA;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
								</xsl:element>
							</xsl:for-each>
						
							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
						</events>
					</xsl:if>
					
					<xsl:text>&#xA;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
				</xsl:element>
				<xsl:text>&#xA;</xsl:text>
			</xsl:for-each>

			<xsl:text>&#xA;</xsl:text>
			<xsl:text>&#009;</xsl:text>
		</interfaces>

	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

		<xsl:call-template name="bn:Generate_Seasoning_Interfaces">
<!--
			<xsl:with-param name ="core_IDs_param"			select ="$rest_core_IDs"></xsl:with-param>
-->
		</xsl:call-template>

	</xsl:template>

</xsl:stylesheet>

