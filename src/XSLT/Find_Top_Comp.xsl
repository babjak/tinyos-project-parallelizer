<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0"
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform"
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Helper_Funcs.xsl"/>

	<xsl:output method="xml" omit-xml-declaration="yes" encoding='UTF-8' />


<!--
######################################################################################################
#
#
#
######################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>


	<xsl:variable	name="doc_root"					select="/"/>

<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Find_All_Contained_Configurations_Rec">

		<xsl:param		name="comps_param"/> <!-- List of components to be checked -->

		<xsl:variable	name="comps_empty"			select="not( boolean( $comps_param ) )"/>

		<xsl:choose>
			<xsl:when test="$comps_empty">
				<!-- Nothing to do -->
				<xsl:sequence	select="()"/>
			</xsl:when>

			<xsl:otherwise>
				<!-- Check first component -->
				<xsl:variable	name="comp"				select="$comps_param[1]"/>
				<xsl:variable	name="rest_comps"		select="$comps_param[position() > 1]"/>

				<xsl:sequence	select="( bn:Get_Contained_Components($comp)[n:configuration], bn:Find_All_Contained_Configurations_Rec($rest_comps) )"/>

			</xsl:otherwise>
		</xsl:choose>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Name_Is_In_List_Rec">

		<xsl:param		name="name_param"/> <!-- List of names to be checked -->
		<xsl:param		name="ref_names_param"/> <!-- List of names to check against-->

		<xsl:variable	name="ref_names_empty"		select="not( boolean( $ref_names_param ) )"/>

		<xsl:choose>
			<xsl:when test="$ref_names_empty">
				<!-- Nothing to do -->
				<xsl:sequence	select="false()"/>
			</xsl:when>

			<xsl:otherwise>
				<!-- Check first component -->
				<xsl:variable	name="ref_name"			select="$ref_names_param[1]"/>
				<xsl:variable	name="rest_ref_names"	select="$ref_names_param[position() > 1]"/>

				<xsl:sequence	select="$name_param=$ref_name or ends-with( $ref_name, concat('.', $name_param) ) or bn:Name_Is_In_List_Rec($name_param, $rest_ref_names)"/>

			</xsl:otherwise>
		</xsl:choose>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">
		<xsl:variable	name="all_confs"		select="$doc_root/n:nesc/n:components/n:component[n:configuration and not(@abstract)]"/>
		<xsl:variable	name="contained_confs"	select="bn:Find_All_Contained_Configurations_Rec($all_confs)"/>

		All configurations: <xsl:value-of	select="$all_confs/@qname" separator=", "/>
		All contained configurations: <xsl:value-of	select="distinct-values($contained_confs/@qname)" separator=", "/>
		All configurations not contained anywhere: <xsl:value-of	select="$all_confs[not( bn:Name_Is_In_List_Rec(@qname, $contained_confs/@qname) )]/@qname" separator=", "/>
	</xsl:template>

</xsl:stylesheet>

