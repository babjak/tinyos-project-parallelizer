<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Helper_Funcs.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="no" encoding='UTF-8' />


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>

	<xsl:variable	name="top_comp"		select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/top_comp/comp[1]/@name]"/>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template name="bn:Generate_Seasoning_Components">

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#009;</xsl:text>
		<components xmlns="">

		<xsl:for-each-group select="document($input_param_file)/params/comp_list/comp" group-by="@core_ID">

			<xsl:variable	name="wires"			select="$top_comp/n:wiring/n:wire"/>
			<xsl:variable	name="top_comp_prefix"	select="concat($top_comp/@qname, '.')"/>
<!--
			<xsl:variable	name="comps"			select="$doc_root/n:nesc/n:components/n:component[@qname = current-group()/@name]"/>
			<xsl:variable	name="ext_comps"		select="bn:Get_Connecting_Components( $comps, $wires )"/>
			<xsl:variable	name="int_comps"		select="(bn:Get_Connecting_Components( $ext_comps, $wires ))[ @qname = current-group()/@name ]"/>
-->
			<xsl:variable	name="copiable_comp_names"	select="document($input_param_file)/params/copiable_comp_list/comp/@name"/>

			<!-- Get all components residing on this core -->
			<xsl:variable	name="comps"			select="$doc_root/n:nesc/n:components/n:component[@qname = current-group()/@name]"/>
			<!-- Get all components residing on this core that are NOT copiable-->
			<xsl:variable	name="comps_no_copy"	select="$comps[ not(@qname = $copiable_comp_names) ]"/>
			<!-- Get all components 1) connecting to comps_no_copy 2) but are not in comps. So, get all external components from other cores connecting to this core -->
			<xsl:variable	name="ext_comps"		select="(bn:Get_Connecting_Components( $comps_no_copy , $wires ))[ not(@qname = $comps/@qname) ]"/>
			<!-- Get all components 1) connecting to ext_comps 2) are in comps_no_copy. So, get all interface components on this core -->
			<xsl:variable	name="int_comps"		select="( bn:Get_Connecting_Components( $ext_comps, $wires ))[ @qname = $comps_no_copy/@qname ]"/>


<!--
			core-ID:	<xsl:value-of select="current-group()[1]/@core_ID"/>
			comps:		<xsl:value-of select="$comps/@qname" separator=", "/>
			ext_comps:	<xsl:value-of select="$ext_comps/@qname" separator=", "/>
			int_comps:	<xsl:value-of select="$int_comps/@qname" separator=", "/>
			comps-int:	<xsl:value-of select="$comps[not(. = $int_comps)]/@qname" separator=", "/>
			comps+int:	<xsl:value-of select="$comps[. = $int_comps]/@qname" separator=", "/>
			SenseForwardP = RFProcessingP:	<xsl:value-of select="$comps[@qname = 'SenseForwardP'] = $comps[@qname = 'RFProcessingP']"/>
-->
			<xsl:for-each select="$comps">
				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:element name="component" namespace="">
					<xsl:attribute name="name"><xsl:value-of select="if ( starts-with(./@qname, $top_comp_prefix) ) then
																		substring-after(./@qname, $top_comp_prefix)
																	else
																		./@qname"/></xsl:attribute>

					<xsl:if test="document($input_param_file)/params/dedicated_comp_list/comp[ @name = current()/@qname ]/@as_interface_of_IP_core">
						<xsl:attribute name="as_interface_of_IP_core"><xsl:value-of select="document($input_param_file)/params/dedicated_comp_list/comp[ @name = current()/@qname ]/@as_interface_of_IP_core"/></xsl:attribute>
					</xsl:if>

					<xsl:if test="./n:instance">
						<xsl:attribute name="real_name"><xsl:value-of select="./n:instance/n:component-ref/@qname"/></xsl:attribute>
						<xsl:attribute name="generic"></xsl:attribute>

						<xsl:if test="./n:instance/n:arguments">
							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:element name="arguments" namespace="">
								<xsl:for-each select="./n:instance/n:arguments/n:value">

									<xsl:text>&#xA;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:text>&#009;</xsl:text>
									<xsl:element name="value" namespace="">
										<xsl:value-of select="substring-after(./@cst,':')"></xsl:value-of>
									</xsl:element>
								</xsl:for-each>

								<xsl:text>&#xA;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
							</xsl:element>
						</xsl:if>
					</xsl:if>

					<xsl:variable name="interfaces"			select="bn:Get_Interfaces_Among_Components(., $comps, $wires)[n:component-ref/@qname = current()/@qname]"/>
					<xsl:variable name="QBMF_interfaces"	select="bn:Get_Interfaces_Among_Components(., $ext_comps, $wires)[n:component-ref/@qname = current()/@qname]"/>

					<xsl:if test="$interfaces">
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:element name="interfaces" namespace="">

							<xsl:for-each select="$interfaces">
								<xsl:text>&#xA;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:element name="interface" namespace="">
									<xsl:attribute name="name_hint">
										<xsl:value-of select="./@name"/>
									</xsl:attribute>
<!--									
									<xsl:attribute name="real_name">
										<xsl:value-of select="./n:instance/n:interfacedef-ref/@qname"/><xsl:if test="./n:instance/n:arguments">&lt;<xsl:value-of select="./n:instance/n:arguments/n:*/n:typename/n:typedef-ref/@name"/>&gt;</xsl:if>
									</xsl:attribute>
-->
									<xsl:attribute name="uses"><xsl:value-of select="if (./@provided='0') then
																						'True'
																					else
																						'False'"/></xsl:attribute>
									<xsl:attribute name="ref"><xsl:value-of select="./@ref"/></xsl:attribute>
								</xsl:element>
							</xsl:for-each>

							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
						</xsl:element>
					</xsl:if>

					<xsl:if test="$QBMF_interfaces">
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:element name="QBMF_interfaces" namespace="">

							<xsl:for-each select="$QBMF_interfaces">
								<xsl:text>&#xA;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:text>&#009;</xsl:text>
								<xsl:element name="interface" namespace="">
									<xsl:attribute name="name_hint">
										<xsl:value-of select="./@name"/>
									</xsl:attribute>
<!--									
									<xsl:attribute name="name">
										<xsl:value-of select="./n:instance/n:interfacedef-ref/@qname"/><xsl:if test="./n:instance/n:arguments">&lt;<xsl:value-of select="./n:instance/n:arguments/n:*/n:typename/n:typedef-ref/@name"/>&gt;</xsl:if>
									</xsl:attribute>
-->
									<xsl:attribute name="uses"><xsl:value-of select="if (./@provided='0') then
																						'True'
																					else
																						'False'"/></xsl:attribute>
									<xsl:attribute name="ref"><xsl:value-of select="./@ref"/></xsl:attribute>
								</xsl:element>
							</xsl:for-each>

							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
						</xsl:element>
					</xsl:if>

					<xsl:text>&#xA;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
				</xsl:element>

				<xsl:text>&#xA;</xsl:text>

			</xsl:for-each>

		</xsl:for-each-group>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#009;</xsl:text>
		</components>

	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

		<xsl:call-template name="bn:Generate_Seasoning_Components">
<!--
			<xsl:with-param name ="core_IDs_param"			select ="$rest_core_IDs"></xsl:with-param>
-->
		</xsl:call-template>

	</xsl:template>


</xsl:stylesheet>

