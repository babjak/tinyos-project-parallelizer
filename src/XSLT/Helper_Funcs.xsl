<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions">

	<xsl:variable	name="doc_root"					select="/"/>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Get_Connecting_Interfaces">

		<xsl:param		name="component_param"/>
		<xsl:param		name="wires_param"/>


		<!-- Get all the interfaces for component -->
		<xsl:variable	name="comp_interfaces"		select="$doc_root/n:nesc/n:interfaces/n:interface[n:component-ref/@qname = $component_param/@qname]"/>

		<!-- Out of the specified wires get those that have the above interfaces on either end -->
		<xsl:variable	name="comp_wires"			select="$wires_param[ (n:from/n:interface-ref/@ref = $comp_interfaces/@ref) 
																			or
																			(n:to/n:interface-ref/@ref = $comp_interfaces/@ref) ]"/>

		<!-- Get interfaces that connect to the above specified wires -->
		<xsl:variable	name="relevant_interfaces"	select="$doc_root/n:nesc/n:interfaces/n:interface[ (@ref = $comp_wires/n:from/n:interface-ref/@ref) 
																										or
																										(@ref = $comp_wires/n:to/n:interface-ref/@ref)]"/>

<!--
		<xsl:variable	name="relevant_interfaces"	select="$comp_interfaces[ (@ref = $comp_wires/n:from/n:interface-ref/@ref) 
																				or
																				(@ref = $comp_wires/n:to/n:interface-ref/@ref)]"/>
-->

		<xsl:sequence	select="$relevant_interfaces"/>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Is_Inter_Core_Wire">

		<xsl:param		name="wire_param"/>
		<xsl:param		name="comps_param"/>

		<xsl:variable	name="from_comp_name"	select="$doc_root/n:nesc/n:interfaces/n:interface[ (@ref = $wire_param/n:from/n:interface-ref/@ref) ]/n:component-ref/@qname"/>
		<xsl:variable	name="to_comp_name"		select="$doc_root/n:nesc/n:interfaces/n:interface[ (@ref = $wire_param/n:to/n:interface-ref/@ref) ]/n:component-ref/@qname"/>

		<xsl:variable	name="from_comp_core_ID"	select="$comps_param[@name = $from_comp_name]/@core_ID"/>
		<xsl:variable	name="to_comp_core_ID"		select="$comps_param[@name = $to_comp_name]/@core_ID"/>

<!--
		<xsl:sequence	select="not( $from_comp_core_ID = $to_comp_core_ID )"/>
-->
		<xsl:sequence	select="not( boolean($from_comp_core_ID[ . = $to_comp_core_ID ]) )"/>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
<!--
	<xsl:function name="bn:Is_Wire_Connecting_Components">

		<xsl:param		name="comps_param"/>
		<xsl:param		name="wire_param"/>

		<xsl:variable	name="from_interface"	select="$doc_root/n:nesc/n:interfaces/n:interface[@ref = $wire_param/n:from/n:interface-ref/@ref]"/>
		<xsl:variable	name="to_interface"		select="$doc_root/n:nesc/n:interfaces/n:interface[@ref = $wire_param/n:to/n:interface-ref/@ref]"/>

		<xsl:sequence	select="($comps_param/@qname = $from_interface/n:component-ref/@qname) and ($comps_param/@qname = $to_interface/n:component-ref/@qname)"/>

	</xsl:function>
-->

<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Get_Interfaces_Among_Components">

		<xsl:param		name="comps_one_param"/>
		<xsl:param		name="comps_two_param"/>
		<xsl:param		name="wires_param"/>

		<!-- Get all the interfaces for component -->
		<xsl:variable	name="comps_one_interfaces"		select="$doc_root/n:nesc/n:interfaces/n:interface[n:component-ref/@qname = $comps_one_param/@qname]"/>
		<xsl:variable	name="comps_two_interfaces"		select="$doc_root/n:nesc/n:interfaces/n:interface[n:component-ref/@qname = $comps_two_param/@qname]"/>

		<!-- Out of the specified wires get those that have the above interfaces on either end -->
		<xsl:variable	name="comp_wires"	select="$wires_param[ ((n:from/n:interface-ref/@ref = $comps_one_interfaces/@ref) 
																	and
																	(n:to/n:interface-ref/@ref = $comps_two_interfaces/@ref))
																	or
																	((n:from/n:interface-ref/@ref = $comps_two_interfaces/@ref) 
																	and
																	(n:to/n:interface-ref/@ref = $comps_one_interfaces/@ref)) ]"/>

		<!-- Get interfaces that connect to the above specified wires -->
		<xsl:variable	name="relevant_interfaces"	select="$doc_root/n:nesc/n:interfaces/n:interface[ (@ref = $comp_wires/n:from/n:interface-ref/@ref) 
																										or
																										(@ref = $comp_wires/n:to/n:interface-ref/@ref)]"/>

<!--
		<xsl:sequence	select="$comps_one_interfaces[@name = $comps_two_interfaces/@name]"/>
-->
		<xsl:sequence	select="$relevant_interfaces"/>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Get_Connecting_Components">

		<xsl:param		name="component_param"/>
		<xsl:param		name="wires_param"/>


		<!-- Get interfaces that are used either by the component or a connected component -->
		<xsl:variable	name="relevant_interfaces"	select="bn:Get_Connecting_Interfaces($component_param, $wires_param)"/>

		<!-- Get the components with the above specified interfaces -->
		<xsl:variable	name="components"			select="$doc_root/n:nesc/n:components/n:component[ @qname = $relevant_interfaces/n:component-ref/@qname ]"/>

<!--
		<xsl:sequence	select="distinct-values( $components[ not (@qname = $component_param/@qname) ] )"/>
-->
		<xsl:sequence	select="$components[ not (@qname = $component_param/@qname) ]"/>

	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Get_Contained_Components">

		<xsl:param		name="component_param"/>


		<xsl:variable	name="component_name"	select="$component_param/@qname"/>


		<xsl:variable	name="all_wires"		select="$component_param/n:wiring/n:wire"/>

		<xsl:variable	name="all_from_refs"	select="$all_wires/n:from/n:interface-ref"/>
		<xsl:variable	name="all_to_refs"		select="$all_wires/n:to/n:interface-ref"/>
		<xsl:variable	name="all_refs"			select="($all_from_refs | $all_to_refs)"/>
<!--		<xsl:variable	name="all_interfaces"	select="key('interfaces_by_ref', $all_refs/@ref)"/> -->
		<xsl:variable	name="all_interfaces"	select="$doc_root/n:nesc/n:interfaces/n:interface[ @ref = $all_refs/@ref ]"/>
		<xsl:variable	name="all_comp_names"	select="distinct-values( $all_interfaces/n:component-ref/@qname[not(. = $component_name)] )"/> <!-- To avoid endless loops, because this contains the component itself -->
<!--		<xsl:variable	name="all_components"	select="key('components_by_name', $all_comp_names)"/> -->
		<xsl:variable	name="all_components"	select="$doc_root/n:nesc/n:components/n:component[ @qname = $all_comp_names]"/>


		<xsl:sequence	select="$all_components"/>

	</xsl:function>	

</xsl:stylesheet>
