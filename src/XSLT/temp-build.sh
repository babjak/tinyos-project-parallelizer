#!/bin/sh

parallelisator_dir=/home/babjak/fontos/Parallel_TinyOS/tinyos-project-parallelisator
project_dir=$parallelisator_dir/Example/SenseForward_project_single_core
temp_dir=$project_dir/../temp
output_name=`echo $1 | sed "s/.xsl/_output.xml/"`
output_log_name=`echo $1 | sed "s/.xsl/_output.log/"`



java -jar $parallelisator_dir/XSLT/saxon9he.jar -s:$temp_dir/wiring-check.xml -xsl:$parallelisator_dir/XSLT/$1 -o:$temp_dir/$output_name input_param_file=$parallelisator_dir/Example/temp/partitioning_guide_full_flat.xml 2> $temp_dir/$output_log_name
