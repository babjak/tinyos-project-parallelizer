<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Generate_Seasoning_Wires.xsl"/>
	<xsl:import href="Generate_Seasoning_Interfaces.xsl"/>
	<xsl:import href="Generate_Seasoning_Components.xsl"/>
	<xsl:import href="Generate_Seasoning_Cores.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="no" encoding='UTF-8' />


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>

	<xsl:variable	name="top_comp"		select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/top_comp/comp[1]/@name]"/>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

<xsl:text>&#xA;</xsl:text>
<xsl:text>&#xA;</xsl:text>
<data xmlns="">

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#xA;</xsl:text>

		<xsl:text>&#009;</xsl:text>
		<top_comp xmlns=""><xsl:value-of select="$top_comp/@qname"/></top_comp>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#xA;</xsl:text>


		<xsl:call-template name="bn:Generate_Seasoning_Wires"/>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#xA;</xsl:text>


		<xsl:call-template name="bn:Generate_Seasoning_Interfaces"/>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#xA;</xsl:text>


		<xsl:call-template name="bn:Generate_Seasoning_Components"/>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#xA;</xsl:text>


		<xsl:call-template name="bn:Generate_Seasoning_Cores"/>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#xA;</xsl:text>
		
</data>

	</xsl:template>

</xsl:stylesheet>

