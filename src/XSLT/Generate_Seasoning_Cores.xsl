<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions"
				xmlns:xs	= "http://www.w3.org/2001/XMLSchema">

	<xsl:import href="Helper_Funcs.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="no" encoding='UTF-8' />


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:param		name="input_param_file"	as="xs:string"	required="yes"/>

	<xsl:variable	name="top_comp"		select="/n:nesc/n:components/n:component[@qname = document($input_param_file)/params/top_comp/comp[1]/@name]"/>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template name="bn:Generate_Seasoning_Cores">

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#009;</xsl:text>
		<cores xmlns="">

		<xsl:for-each-group select="document($input_param_file)/params/comp_list/comp" group-by="@core_ID">

			<xsl:variable	name="wires"			select="$top_comp/n:wiring/n:wire"/>
			<xsl:variable	name="top_comp_prefix"	select="concat($top_comp/@qname, '.')"/>

			<xsl:variable	name="copiable_comp_names"	select="document($input_param_file)/params/copiable_comp_list/comp/@name"/>

			<!-- Get all components residing on this core -->
			<xsl:variable	name="comps"			select="$doc_root/n:nesc/n:components/n:component[@qname = current-group()/@name]"/>
			<!-- Get all components residing on this core that are NOT copiable-->
			<xsl:variable	name="comps_no_copy"	select="$comps[ not(@qname = $copiable_comp_names) ]"/>
			<!-- Get all components 1) connecting to comps_no_copy 2) but are not in comps. So, get all external components from other cores connecting to this core -->
			<xsl:variable	name="ext_comps"		select="(bn:Get_Connecting_Components( $comps_no_copy , $wires ))[ not(@qname = $comps/@qname) ]"/>
			<!-- Get all components 1) connecting to ext_comps 2) are in comps_no_copy. So, get all interface components on this core -->
			<xsl:variable	name="int_comps"		select="( bn:Get_Connecting_Components( $ext_comps, $wires ))[ @qname = $comps_no_copy/@qname ]"/>
			<!-- Get all components on this core that are not interfaces -->
			<xsl:variable	name="local_comps"		select="$comps[not(./@qname = $int_comps/@qname)]"/>

			<xsl:text>&#xA;</xsl:text>
			<xsl:text>&#009;</xsl:text>
			<xsl:text>&#009;</xsl:text>
			<xsl:element name="core" namespace="">
				<xsl:attribute name="name"><xsl:value-of select="current-group()[1]/@core_ID"/></xsl:attribute>
				<xsl:if test="document($input_param_file)/params/core_list/core[@ID = current-group()[1]/@core_ID]/@dontgenerate">
					<xsl:attribute name="dont_generate">True</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="number"><xsl:value-of select="document($input_param_file)/params/core_list/core[@ID = current-group()[1]/@core_ID]/@number"/></xsl:attribute>

				<xsl:if test="$local_comps">

					<xsl:text>&#xA;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:element name="local_components" namespace="">

						<xsl:for-each select="$local_comps">
							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:element name="component" namespace="">
								<xsl:attribute name="name"><xsl:value-of select="if ( starts-with(./@qname, $top_comp_prefix) ) then
																					substring-after(./@qname, $top_comp_prefix)
																				else
																					./@qname"/></xsl:attribute>
	<!--							
								<xsl:attribute name="name"><xsl:value-of select="if (./n:instance) then
																					./n:instance/n:component-ref/@qname
																				else
																					./@qname"/></xsl:attribute>
	-->
							</xsl:element>

						</xsl:for-each>
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
					</xsl:element>

				</xsl:if>


				<xsl:if test="$int_comps">

					<xsl:text>&#xA;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:element name="local_QBMF_components" namespace="">

						<xsl:for-each select="$int_comps">
							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:element name="component" namespace="">
								<xsl:attribute name="name"><xsl:value-of select="if ( starts-with(./@qname, $top_comp_prefix) ) then
																					substring-after(./@qname, $top_comp_prefix)
																				else
																					./@qname"/></xsl:attribute>
							</xsl:element>

						</xsl:for-each>
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
					</xsl:element>

				</xsl:if>

				<xsl:if test="$ext_comps">

					<xsl:text>&#xA;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:text>&#009;</xsl:text>
					<xsl:element name="remote_QBMF_components" namespace="">

						<xsl:for-each select="$ext_comps">
							<xsl:text>&#xA;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:text>&#009;</xsl:text>
							<xsl:element name="component" namespace="">
								<xsl:attribute name="name"><xsl:value-of select="if ( starts-with(./@qname, $top_comp_prefix) ) then
																					substring-after(./@qname, $top_comp_prefix)
																				else
																					./@qname"/></xsl:attribute>
							</xsl:element>

						</xsl:for-each>
						<xsl:text>&#xA;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
						<xsl:text>&#009;</xsl:text>
					</xsl:element>

				</xsl:if>

				<xsl:text>&#xA;</xsl:text>
				<xsl:text>&#009;</xsl:text>
				<xsl:text>&#009;</xsl:text>
			</xsl:element>
			<xsl:text>&#xA;</xsl:text>

		</xsl:for-each-group>

		<xsl:text>&#xA;</xsl:text>
		<xsl:text>&#009;</xsl:text>
		</cores>

	</xsl:template>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

		<xsl:call-template name="bn:Generate_Seasoning_Cores">
<!--
			<xsl:with-param name ="core_IDs_param"			select ="$rest_core_IDs"></xsl:with-param>
-->
		</xsl:call-template>

	</xsl:template>

</xsl:stylesheet>

