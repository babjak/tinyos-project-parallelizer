<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	version		= "2.0" 
				xmlns		= "http://graphml.graphdrawing.org/xmlns"
				xmlns:xsl	= "http://www.w3.org/1999/XSL/Transform" 
				xmlns:n		= "http://www.tinyos.net/nesC"
				xmlns:bn	= "http://Bens_NesC_XML_dump_analysis_helper_functions">

	<xsl:import href="./Helper_Funcs.xsl"/>


	<xsl:output method="xml" omit-xml-declaration="no" encoding='UTF-8' />

	
	<xsl:key name="components_by_name"                  match="n:component"             use="@qname"/>
	<xsl:key name="components_by_wire_from"				match="n:component"             use="n:wiring/n:wire/n:from/n:interface-ref/@ref"/>
	<xsl:key name="components_by_wire_to"				match="n:component"             use="n:wiring/n:wire/n:to/n:interface-ref/@ref"/>
	<xsl:key name="wires_by_from_interface_ref"         match="n:nesc/n:wiring/n:wire"  use="n:from/n:interface-ref/@ref"/>		
	<xsl:key name="wires_by_to_interface_ref"           match="n:nesc/n:wiring/n:wire"  use="n:to/n:interface-ref/@ref"/>		
	<xsl:key name="interfaces_by_component_ref_qname"   match="n:interface"             use="n:component-ref/@qname"/>		
	<xsl:key name="interfaces_by_ref"                   match="n:interface"             use="@ref"/>		


	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	
	
<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Is_Component_Reused">
		<xsl:param name="component_name_param"				/>
		<xsl:param name="reused_components_param"			/>
		<xsl:param name="containing_component_name_param"	/>	
	
		<xsl:sequence	select="boolean( $reused_components_param[ @qname = $component_name_param ] ) and not($containing_component_name_param = '')"/>
	</xsl:function>	


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Correct_Component_Name">
		<xsl:param name="component_name_param"				/>	
		<xsl:param name="component_reused_param"			/>
		<xsl:param name="containing_component_name_param"	/>	
	
		<xsl:sequence	select="if ( $component_reused_param ) then
									concat($component_name_param, ' in ', $containing_component_name_param)
								else
									$component_name_param"/>
	</xsl:function>	
	

<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template name="handle_component_rec">

		<xsl:param		name="component_name_param"				/>
		<xsl:param		name="reused_components_param"			/>
		<xsl:param		name="containing_component_name_param"	select="''"/>
		<xsl:param		name="level"	select="0"/>

		<xsl:variable	name="component_reused"			select="bn:Is_Component_Reused($component_name_param, 
																					$reused_components_param,
																					$containing_component_name_param)"/>
																					
		<xsl:variable	name="component_name"			select="bn:Correct_Component_Name($component_name_param,
																					$component_reused,
																					$containing_component_name_param)"/>
																					
		<xsl:variable	name="component"				select="key('components_by_name', $component_name_param)[1]"/>

		
		<xsl:element name="node">
		
			<xsl:attribute name="id"><xsl:value-of select="$component_name"/></xsl:attribute>

			<data key="d1"><xsl:value-of select="$component_name"/></data>
			
			
			<!-- If configuration -->
			<xsl:if test="$component/n:configuration">

				<xsl:element name="graph">

					<xsl:attribute name="edgedefault">directed</xsl:attribute>

					<xsl:attribute name="id"><xsl:value-of select="$component_name"/>:</xsl:attribute>


					<xsl:if test="not( $component_reused )">

						<xsl:variable	name="all_wires"		select="$component/n:wiring/n:wire"/>

<!--						
						<xsl:variable	name="all_from_refs"	select="$all_wires/n:from/n:interface-ref"/>
						<xsl:variable	name="all_to_refs"		select="$all_wires/n:to/n:interface-ref"/>
						<xsl:variable	name="all_refs"			select="($all_from_refs | $all_to_refs)"/>
						<xsl:variable	name="all_interfaces"	select="key('interfaces_by_ref', $all_refs/@ref)"/>
						<xsl:variable	name="all_comp_names"	select="distinct-values($all_interfaces/n:component-ref/@qname[not(. = $component_name_param)])"/>
						!- To avoid endless loops, because this contains the component itself -
						<xsl:variable	name="all_components"	select="key('components_by_name', $all_comp_names)"/>
-->
						<xsl:variable	name="all_components"	select="bn:Get_Contained_Components($component)"/>
						
<!--						
						<xsl:value-of select="$all_components/@qname" separator=","/>
-->
						
						<xsl:for-each select="$all_components">
						
<!--						
							Starting <xsl:value-of select="@qname"/> at level <xsl:value-of select="$level"/>:
							<br/>
-->
							<xsl:call-template name="handle_component_rec">
								<xsl:with-param name="component_name_param"				select="@qname"/>	
								<xsl:with-param name="reused_components_param"			select="$reused_components_param"/>	
								<xsl:with-param name="containing_component_name_param"	select="$component_name_param"/>
								<xsl:with-param name="level"							select="$level + 1"/>
							</xsl:call-template>
<!--
							<br/>
-->							
						</xsl:for-each>
						
						<xsl:for-each select="$all_wires">
				
							<xsl:variable name="wire_from_ref"	select="n:from/n:interface-ref/@ref"/> 
							<xsl:variable name="wire_to_ref"	select="n:to/n:interface-ref/@ref"/> 

							<xsl:variable name="from_name"		select="key('interfaces_by_ref', $wire_from_ref)/n:component-ref/@qname"/>
							<xsl:variable name="reused"			select="bn:Is_Component_Reused($from_name, 
																					$reused_components_param[not(. = $component)],
																					$component_name)"/>
																					
							<xsl:variable name="from_name"		select="bn:Correct_Component_Name($from_name,
																					$reused,
																					$component_name)"/>

							<xsl:variable name="to_name"		select="key('interfaces_by_ref', $wire_to_ref)/n:component-ref/@qname"/>
							<xsl:variable name="reused"			select="bn:Is_Component_Reused($to_name, 
																					$reused_components_param[not(. = $component)],
																					$component_name)"/>
																					
							<xsl:variable name="to_name"		select="bn:Correct_Component_Name($to_name,
																					$reused,
																					$component_name)"/>
				
							<xsl:element name="edge">
					
								<xsl:attribute name="source">
									<xsl:value-of select="$from_name"/>
								</xsl:attribute>
				
								<xsl:attribute name="target">
									<xsl:value-of select="$to_name"/>
								</xsl:attribute>
						
								<data key="d0">
									<xsl:value-of select="$from_name"/> -&gt; <xsl:value-of select="$to_name"/>
								</data>						
						
							</xsl:element>
				
						</xsl:for-each>

					</xsl:if>

				</xsl:element>	

			</xsl:if>

		</xsl:element>		

	</xsl:template>
	

<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:function name="bn:Is_Config_Reused">
		<xsl:param name="comp"/>
		<xsl:param name="components"/>
		<xsl:param name="interfaces"/>

		<xsl:variable	name="comp_name"					select="$comp/@qname"/>
		<xsl:variable	name="all_interfaces_for_comp"		select="$interfaces[ n:component-ref/@qname = $comp_name ]"/>
		<xsl:variable	name="containing_configs"			select="$components[ n:wiring/n:wire/n:from/n:interface-ref/@ref = $all_interfaces_for_comp/@ref or n:wiring/n:wire/n:to/n:interface-ref/@ref = $all_interfaces_for_comp/@ref ]"/>
		<xsl:variable	name="containing_configs_no_self"	select="$containing_configs[ not(@qname = $comp_name) ]"/>

		<xsl:sequence										select="boolean( count($containing_configs_no_self) &gt; 1 )"/>
	</xsl:function>


<!--
##########################################################################################################################
#
#
#
##########################################################################################################################
-->
	<xsl:template match="n:nesc">

		<graphml	xmlns	= "http://graphml.graphdrawing.org/xmlns"
					xmlns:y	= "http://www.yworks.com/xml/graphml">


			<key attr.name="url" attr.type="string" for="edge" id="d0"/>
			<key attr.name="url" attr.type="string" for="node" id="d1"/>


			<graph	id			="G"
					edgedefault	="directed">

				<xsl:variable	name="reused_components"	select="n:components/n:component[not(@abstract) and (bn:Is_Config_Reused(., current()/n:components/n:component, current()/n:interfaces/n:interface) or @qname='SenseForwardAppC')]"/>
									
<!--									
				<xsl:value-of select="$reused_components/@qname"/>
				
				<br />
-->
					
				<!-- Nodes -->
				<xsl:for-each select="$reused_components">
<!--
					<xsl:value-of select="$reused_components[not( . = current() )]/@qname"/>

					<br />
-->

<!--
					Starting <xsl:value-of select="@qname"/>:
					<br/>
-->
<!--
					<xsl:if test="not(@qname = 'CC2420RadioC')">
-->					
						<xsl:call-template name="handle_component_rec">
							<xsl:with-param name="component_name_param"		select="@qname"/>	
							<xsl:with-param name="reused_components_param"	select="$reused_components"/>	
						</xsl:call-template>
<!--						
					</xsl:if>
-->					
<!--					
					<br/>
-->					
				</xsl:for-each>

				
				<!-- Edges -->
				<!--
				<xsl:for-each select="n:wiring/n:wire">
				
					<xsl:variable name="wire_from_ref" select="n:from/n:interface-ref/@ref"/> 
					<xsl:variable name="wire_to_ref" select="n:to/n:interface-ref/@ref"/> 

					<xsl:element name="edge">
					
						<xsl:attribute name="source">
							<xsl:value-of select="key('interfaces_by_ref', $wire_from_ref)/n:component-ref/@qname"/>
						</xsl:attribute>
				
						<xsl:attribute name="target">
							<xsl:value-of select="key('interfaces_by_ref', $wire_to_ref)/n:component-ref/@qname"/>
						</xsl:attribute>
						
						<data key="d0">
							<xsl:value-of select="key('interfaces_by_ref', $wire_from_ref)/@name"/> -&gt; <xsl:value-of select="key('interfaces_by_ref', $wire_to_ref)/@name"/>
						</data>						
						
					</xsl:element>
				
				</xsl:for-each>
				-->

			</graph>

		</graphml>			

	</xsl:template>

</xsl:stylesheet>
