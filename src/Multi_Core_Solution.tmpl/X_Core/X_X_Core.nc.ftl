<#assign core_name = core.@name>
#include "Msg_Queue_Format.h"
<#-- TODO headers? 
${core.raw_lines_header[0]!""}
-->

configuration ${seasoning.data.top_comp}_${core.@name}_Core
{
}
implementation
{
////////////////////////////////////////////////////////////////////////
// Multi core extra
// Very likely initialization is needed

	components MainC;


////////////////////////////////////////////////////////////////////////
// Instantiate all the core's local components
// (except for MainC, which we have already taken care of)

<#list seasoning["data/components/component[@name = /data/cores/core[@name = $core_name]/local_components/component/@name | /data/cores/core[@name = $core_name]/local_QBMF_components/component/@name]"] as comp>
	<#if comp.@generic[0]??>
		<#if comp.@real_name[0]?? && comp.@real_name != comp.@name>
	components new ${comp.@real_name}(<#list comp.arguments.value as value>${value}<#if value_has_next>, </#if></#list>) as ${comp.@name};
		<#else>
	components new ${comp.@name}(<#list comp.arguments.value as value>${value}<#if value_has_next>, </#if></#list>);
		</#if>
	<#else>
		<#if comp.@real_name[0]?? && comp.@real_name != comp.@name && comp.@real_name != "MainC">
	components ${comp.@real_name} as ${comp.@name};
		<#elseif comp.@name != "MainC">
	components ${comp.@name};
		</#if>
	</#if>
</#list>


////////////////////////////////////////////////////////////////////////
// Instantiate wrapper components for remote QBMF components on other cores

<#list core.remote_QBMF_components.component as comp>
	components ${comp.@name}_W		as ${comp.@name};
</#list>


////////////////////////////////////////////////////////////////////////
// Wire up all local components among themselves

<#list seasoning.data.wires.wire as wire>
	<#assign from_interf_ref	= wire.from.interface_ref.@ref>
	<#assign to_interf_ref		= wire.to.interface_ref.@ref>
	<#if seasoning.data.components.component.interfaces.interface.@ref?seq_contains(from_interf_ref) && seasoning.data.components.component.interfaces.interface.@ref?seq_contains(to_interf_ref)>
		<#assign from_comp			= seasoning["/data/components/component[interfaces/interface/@ref = $from_interf_ref]"][0]>
		<#assign to_comp 			= seasoning["/data/components/component[interfaces/interface/@ref = $to_interf_ref]"][0]>
		<#assign local_comps 		= core.local_components.component>
		<#assign local_QBMF_comps 	= core.local_QBMF_components.component>
		<#if (local_comps.@name?seq_contains(from_comp.@name) || local_QBMF_comps.@name?seq_contains(from_comp.@name)) && (local_comps.@name?seq_contains(to_comp.@name) || local_QBMF_comps.@name?seq_contains(to_comp.@name))>
			<#assign from_interf_name 	= wire.from.interface_ref.@name_hint>
			<#assign to_interf_name 	= wire.to.interface_ref.@name_hint>
	${from_comp.@name}.${from_interf_name} -> ${to_comp.@name}<#if from_interf_name != to_interf_name>.${to_interf_name}</#if>;
		</#if>
	</#if>
</#list>


////////////////////////////////////////////////////////////////////////
// Wire up local QBMF component with wrappers for remote QBMF components

<#list seasoning.data.wires.wire as wire>
	<#assign from_interf_ref 	= wire.from.interface_ref.@ref>
	<#assign to_interf_ref 		= wire.to.interface_ref.@ref>
	<#if seasoning.data.components.component.QBMF_interfaces.interface.@ref?seq_contains(from_interf_ref) && seasoning.data.components.component.QBMF_interfaces.interface.@ref?seq_contains(to_interf_ref)>
		<#assign from_comp 			= seasoning["/data/components/component[QBMF_interfaces/interface/@ref = $from_interf_ref]"]>
		<#assign to_comp 			= seasoning["/data/components/component[QBMF_interfaces/interface/@ref = $to_interf_ref]"]>
		<#assign local_QBMF_comps 	= core.local_QBMF_components.component>
		<#assign remote_QBMF_comps 	= core.remote_QBMF_components.component>
		<#if (local_QBMF_comps.@name?seq_contains(from_comp.@name) && remote_QBMF_comps.@name?seq_contains(to_comp.@name)) || (local_QBMF_comps.@name?seq_contains(to_comp.@name) && remote_QBMF_comps.@name?seq_contains(from_comp.@name))>
			<#assign from_interf_name 	= wire.from.interface_ref.@name_hint>
			<#assign to_interf_name 	= wire.to.interface_ref.@name_hint>
	${from_comp.@name}.${from_interf_name} -> ${to_comp.@name}<#if from_interf_name != to_interf_name>.${to_interf_name}</#if>;
		</#if>
	</#if>
</#list>


////////////////////////////////////////////////////////////////////////
// Multi core extra
// Message passing

	components QBMFC;

<#list core.remote_QBMF_components.component as comp>
	${comp.@name}.RXMsg -> QBMFC.To_${comp.@name};
	${comp.@name}.TXMsg -> QBMFC.From_${comp.@name};

</#list>

	QBMFC.Boot -> MainC;
}
