<@pp.dropOutputFile />
<#list seasoning["data/cores/core[ not(@dont_generate='True') ]"] as core>
<#list core.remote_QBMF_components.component as remote_comp>
<#assign  remote_comp_name = remote_comp.@name>
<@pp.changeOutputFile name="/" + core.@name + "_Core/Wrappers/" + remote_comp.@name + "_W.nc" />
#include "Msg_Queue_Types.h"

module ${remote_comp.@name}_W
{
<#list core.local_QBMF_components.component as local_comp>
	<#assign local_comp_name	= local_comp.@name>
	<#assign wire 				= seasoning["data/wires/wire[*/interface_ref/@ref = /data/components/component[@name = $local_comp_name]/QBMF_interfaces/interface/@ref and */interface_ref/@ref = /data/components/component[@name = $remote_comp_name]/QBMF_interfaces/interface/@ref]"]>
	<#assign from_interf_ref	= wire.from.interface_ref.@ref>
	<#assign to_interf_ref		= wire.to.interface_ref.@ref>
	<#assign comp_interf		= seasoning["data/components/component[@name = $remote_comp_name]/QBMF_interfaces/interface[ @ref = $from_interf_ref or @ref = $to_interf_ref ]"]>
	<#assign interf				= seasoning["data/interfaces/interface[ @from_ref = $from_interf_ref and @to_ref = $to_interf_ref ]"]>
	<#if comp_interf.@uses == "True">uses	<#else>provides</#if>	interface	<#if interf.@real_name?is_string>${interf.@real_name} as </#if>${comp_interf.@name_hint};
</#list>

	uses		interface	RXMsg;
	uses		interface	TXMsg;
}
implementation
{
	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	event void RXMsg.receive( error_t			err,
								MsgID_t*		p_msg_ID,
								nx_uint8_t*		p_data,
								unsigned short	data_size )
	{
<#list core.local_QBMF_components.component as local_comp>
	<#assign local_comp_name	= local_comp.@name>
	<#assign wire 				= seasoning["data/wires/wire[*/interface_ref/@ref = /data/components/component[@name = $local_comp_name]/QBMF_interfaces/interface/@ref and */interface_ref/@ref = /data/components/component[@name = $remote_comp_name]/QBMF_interfaces/interface/@ref]"]>
	<#assign from_interf_ref 	= wire.from.interface_ref.@ref>
	<#assign to_interf_ref 		= wire.to.interface_ref.@ref>
	<#assign comp_interf 		= seasoning["data/components/component[@name = $remote_comp_name]/QBMF_interfaces/interface[ (@ref = $from_interf_ref or @ref = $to_interf_ref) ]"]>
	<#assign interf 			= seasoning["data/interfaces/interface[ @from_ref = $from_interf_ref and @to_ref = $to_interf_ref ]"]>
	<#if comp_interf.@uses == "True">
		<#list interf.commands.command as command>
		if (p_msg_ID->interfID	== ${Bens_funcs.Get_Interface_Designator(from_interf_ref)} &&
			p_msg_ID->funcID 	== COM_${command.@name?upper_case})
		{
			typedef struct buffer_struct_s
			{
			<#list command.func_params.param as param>
				${param.@type} ${param.@name};
			</#list>
			} buffer_struct_t;

			call ${comp_interf.@name_hint}.${command.@name}( <#list command.func_params.param as param>((buffer_struct_t*) p_data)->${param.@name}<#if param_has_next>, </#if></#list> );

			return;
		}
		</#list>
	<#else>
		<#list interf.events.event as event>
		if (p_msg_ID->interfID	== ${Bens_funcs.Get_Interface_Designator(from_interf_ref)} &&
			p_msg_ID->funcID 	== SIG_${event.@name?upper_case})
		{
			typedef struct buffer_struct_s
			{
			<#list event.func_params.param as param>
				${param.@type} ${param.@name};
			</#list>
			} buffer_struct_t;

			signal ${comp_interf.@name_hint}.${event.@name}( <#list event.func_params.param as param>((buffer_struct_t*) p_data)->${param.@name}<#if param_has_next>, </#if></#list> );

			return;
		}
		</#list>
	</#if>
</#list>
	}

<#list core.local_QBMF_components.component as local_comp>
	<#assign local_comp_name	= local_comp.@name>
	<#assign wire 				= seasoning["data/wires/wire[*/interface_ref/@ref = /data/components/component[@name = $local_comp_name]/QBMF_interfaces/interface/@ref and */interface_ref/@ref = /data/components/component[@name = $remote_comp_name]/QBMF_interfaces/interface/@ref]"]>
	<#assign from_interf_ref 	= wire.from.interface_ref.@ref>
	<#assign to_interf_ref 		= wire.to.interface_ref.@ref>
	<#assign comp_interf 		= seasoning["data/components/component[@name = $remote_comp_name]/QBMF_interfaces/interface[ (@ref = $from_interf_ref or @ref = $to_interf_ref) ]"]>
	<#assign interf 			= seasoning["data/interfaces/interface[ @from_ref = $from_interf_ref and @to_ref = $to_interf_ref ]"]>
	<#if comp_interf.@uses == "True">
		<#list interf.events.event as event>
	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	event ${event.return.@type[0]!"void"} ${comp_interf.@name_hint}.${event.@name}(<#list event.func_params.param as param>${param.@type} ${param.@name}<#if param_has_next>, </#if></#list>)
	{
		typedef struct buffer_struct_s
		{
		<#list event.func_params.param as param>
			${param.@type} ${param.@name};
		</#list>
		} buffer_struct_t;

		buffer_struct_t buffer_struct;


		MsgID_t MsgID;


		///////////////////////////
		<#list event.func_params.param as param>
		buffer_struct.${param.@name} = ${param.@name};
		</#list>

		MsgID.compID	= COMP_${local_comp.@name?upper_case};
		MsgID.interfID	= ${Bens_funcs.Get_Interface_Designator(from_interf_ref)};
		MsgID.funcID	= SIG_${event.@name?upper_case};
		
		call TXMsg.send( &MsgID, (nx_uint8_t*) &buffer_struct, sizeof(buffer_struct_t) );

		<#if event.return.@default_val[0]??>return ${event.return.@default_val};</#if>
	}

		</#list>
	<#else>
		<#list interf.commands.command as command>
	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	command ${command.return.@type[0]!"void"} ${comp_interf.@name_hint}.${command.@name}(<#list command.func_params.param as param>${param.@type} ${param.@name}<#if param_has_next>, </#if></#list>)
	{
		typedef struct buffer_struct_s
		{
		<#list command.func_params.param as param>
			${param.@type} ${param.@name};
		</#list>
		} buffer_struct_t;

		buffer_struct_t buffer_struct;


		MsgID_t MsgID;


		///////////////////////////
		<#list command.func_params.param as param>
		buffer_struct.${param.@name} = ${param.@name};
		</#list>

		MsgID.compID	= COMP_${local_comp.@name?upper_case};
		MsgID.interfID	= ${Bens_funcs.Get_Interface_Designator(from_interf_ref)};
		MsgID.funcID	= COM_${command.@name?upper_case};
		
		call TXMsg.send( &MsgID, (nx_uint8_t*) &buffer_struct, sizeof(buffer_struct_t) );

		<#if command.return.@default_val[0]??>return ${command.return.@default_val};</#if>
	}

		</#list>
	</#if>
</#list>
}
</#list>
</#list>