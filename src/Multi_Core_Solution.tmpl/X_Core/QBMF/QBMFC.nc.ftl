<@pp.dropOutputFile />
<#list seasoning["data/cores/core[ not(@dont_generate='True') ]"] as core>
<@pp.changeOutputFile name="/" + core.@name + "_Core/QBMF/QBMFC.nc" />
#include "Msg_Queue_Format.h"
#include <Timer.h>
#include "Msg_Queue_ID.h"


configuration QBMFC
{
<#list core.remote_QBMF_components.component as comp>
	provides	interface	RXMsg			as	To_${comp.@name};
	provides	interface	TXMsg			as	From_${comp.@name};

</#list>
	uses		interface	Boot;
}
implementation 
{
	components MsgHandlerP;
	components new TimerMilliC()	as Timer_Msg_Q_Poll_C;
	components HWRXMsgQP;
	components HWTXMsgQP;

	MsgHandlerP.Timer_Msg_Q_Poll_IF	-> Timer_Msg_Q_Poll_C.Timer;

	MsgHandlerP.FromRXMsgQ			-> HWRXMsgQP.RXMsgSource;
	MsgHandlerP.FromRXMsgQ_misc		-> HWRXMsgQP.RXMsgMisc;
	MsgHandlerP.ToTXMsgQ			-> HWTXMsgQP.TXMsgDest;

<#list core.remote_QBMF_components.component as comp>
	To_${comp.@name}			= MsgHandlerP.To_${comp.@name};
	From_${comp.@name}			= MsgHandlerP.From_${comp.@name};

</#list>
	Boot								= MsgHandlerP.Boot;
}
</#list>