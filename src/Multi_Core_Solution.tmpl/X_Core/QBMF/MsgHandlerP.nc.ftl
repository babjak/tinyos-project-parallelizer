<@pp.dropOutputFile />
<#list seasoning["data/cores/core[ not(@dont_generate='True') ]"] as core>
<#assign core_name = core.@name>
<@pp.changeOutputFile name="/" + core_name + "_Core/QBMF/MsgHandlerP.nc" />

//Create messages from buffers coming from components
//Create buffers from messages (coming from FIFO)
#include "AvroraPrint.h"

module MsgHandlerP
{
	uses		interface	TXMsgDest		as	ToTXMsgQ;
	uses		interface	RXMsgSource		as	FromRXMsgQ;
	uses		interface	RXMsgMisc		as	FromRXMsgQ_misc;

<#list core.remote_QBMF_components.component as remote_comp>
	provides	interface	RXMsg			as	To_${remote_comp.@name};
	provides	interface	TXMsg			as	From_${remote_comp.@name};

</#list>
	uses		interface	Boot;
	uses		interface	Timer<TMilli>	as	Timer_Msg_Q_Poll_IF;
}
implementation
{
	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	event void Boot.booted() 
	{
		call FromRXMsgQ_misc.Post_Msg_Queue_check();
	}

	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	event void Timer_Msg_Q_Poll_IF.fired() 
	{
		call FromRXMsgQ_misc.Post_Msg_Queue_check();
	}

	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	event void FromRXMsgQ.receive( error_t			err,
									nx_uint8_t		source_ID,
									MsgID_t*		p_msg_ID,
									nx_uint8_t*		p_data,
									unsigned short	data_size )
	{
<#list seasoning["data/cores/core[ not(@name = $core_name) ]"] as remote_core>
	<#if Bens_funcs.Do_Sets_Intersect(remote_core.local_QBMF_components.component.@name, core.remote_QBMF_components.component.@name)>
		<#assign comp_list = remote_core["local_QBMF_components/component[ @name = /data/cores/core[@name = $core_name]/remote_QBMF_components/component/@name ]"]>

		printStr("FromRXMsgQ source_ID: ");
	  	printInt8(source_ID);
/*
		printStr("For reference: ");
		printStr("CORE_SENSE: ");
	  	printInt8(CORE_SENSE);
		printStr("CORE_RF: ");
	  	printInt8(CORE_RF);
		printStr("CORE_MAIN: ");
	  	printInt8(CORE_MAIN);
		printStr("CORE_IP_PROC: ");
	  	printInt8(CORE_IP_PROC);
*/

		if ( source_ID == CORE_${remote_core.@name?upper_case} )
		{
		<#list comp_list as comp>
			if ( p_msg_ID->compID == COMP_${comp.@name?upper_case} )
			{
				signal To_${comp.@name}.receive( err, 
									p_msg_ID, 
									p_data, 
									data_size );
				return;
			}
		</#list>
		}
	</#if>
</#list>
	}


	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	event void FromRXMsgQ_misc.No_New_Msg()
	{
		call Timer_Msg_Q_Poll_IF.startOneShot(POLL_PERIOD);
	}

<#list core.remote_QBMF_components.component as remote_comp>
	<#assign remote_comp_name = remote_comp.@name>
	<#assign remote_core = seasoning["data/cores/core[ local_QBMF_components/component/@name = $remote_comp_name ]"]>
	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	command error_t From_${remote_comp.@name}.send( MsgID_t*		p_msg_ID, 
								nx_uint8_t*		p_data,
								unsigned short	data_size )
	{
		nx_uint8_t	dest_ID;
		dest_ID = CORE_${remote_core.@name?upper_case};

		printStr("From_${remote_comp.@name} dest_ID: ");
	  	printInt8(dest_ID);
/*
		printStr("For reference: ");
		printStr("CORE_SENSE: ");
	  	printInt8(CORE_SENSE);
		printStr("CORE_RF: ");
	  	printInt8(CORE_RF);
		printStr("CORE_MAIN: ");
	  	printInt8(CORE_MAIN);
		printStr("CORE_IP_PROC: ");
	  	printInt8(CORE_IP_PROC);
*/

	<#list core.local_QBMF_components.component as local_comp>
		<#assign local_comp_name	= local_comp.@name>
		<#assign wire 				= seasoning["data/wires/wire[*/interface_ref/@ref = /data/components/component[@name = $local_comp_name]/QBMF_interfaces/interface/@ref and */interface_ref/@ref = /data/components/component[@name = $remote_comp_name]/QBMF_interfaces/interface/@ref]"]>
		<#assign from_interf_ref 	= wire.from.interface_ref.@ref>
		if ( p_msg_ID->compID	== COMP_${local_comp.@name?upper_case} &&
			p_msg_ID->interfID	== ${Bens_funcs.Get_Interface_Designator(from_interf_ref)} )
		{
			return call ToTXMsgQ.send( dest_ID,
										p_msg_ID, 
										p_data, 
										data_size );
		}
	</#list>

		return FAIL;
	}
</#list>
}
</#list>