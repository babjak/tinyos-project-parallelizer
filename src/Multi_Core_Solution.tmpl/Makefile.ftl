.PHONY: multi_core_solution

<#list seasoning["data/cores/core[not(./@dont_generate='True')]/@name"] as core_name>
${core_name}.elf:
	cd ${core_name}_Core && $(MAKE) micaz
	cp ./${core_name}_Core/build/micaz/main.exe ./${core_name}.elf
#	$(MAKE) -C ${core_name}_Core
</#list>

elves = <#list seasoning["data/cores/core[not(./@dont_generate='True')]/@name"] as core_name>${core_name}.elf </#list>
multi_core_solution: $(elves)
