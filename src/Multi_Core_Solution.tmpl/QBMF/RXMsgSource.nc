interface RXMsgSource
{
    event void receive( error_t			err,
						nx_uint8_t		source_ID,	//origin of message (ID of transmitter NOT CORE!) THIS WILL GET FILLED IN AUTOMATICALLY BY THE HARDWARE WHEN SENDING MSG.
						MsgID_t*		p_msg_ID, 
						nx_uint8_t*		p_data,
						unsigned short	data_size);
}
