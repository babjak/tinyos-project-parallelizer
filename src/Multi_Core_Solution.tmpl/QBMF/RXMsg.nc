interface RXMsg
{
    event void receive( error_t			err,
						MsgID_t*		p_msg_ID, 
						nx_uint8_t*		p_data,
						unsigned short	data_size);
}
