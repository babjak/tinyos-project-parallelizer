#if defined(MSG_TYPES_H)
#else
#define MSG_TYPES_H

#include <message.h>
#include <TinyError.h>

enum
{
<#list seasoning.data.cores.core.local_QBMF_components.component as comp>
	COMP_${comp.@name?upper_case}<#if comp_has_next>,</#if>
</#list>
};


enum
{
<#list seasoning["data/components/component/QBMF_interfaces/interface[@uses='False' and not(@name_from=preceding::interface/@name_from) and not(@name_to=preceding::interface/@name_to)]"] as interf>
	${Bens_funcs.Get_Interface_Designator(interf.@ref)}<#if interf_has_next>,</#if>
</#list>
};


enum
{
<#list seasoning["data/interfaces/interface/commands/command[ not(@name=preceding::command/@name) ]/@name"] as command_name>
	COM_${command_name?upper_case}<#if command_name_has_next>,</#if>
</#list>
};


enum 
{ 
<#list seasoning["data/interfaces/interface/events/event[ not(@name=preceding::event/@name) ]/@name"] as event_name>
	SIG_${event_name?upper_case}<#if event_name_has_next>,</#if>
</#list>
};

#endif
