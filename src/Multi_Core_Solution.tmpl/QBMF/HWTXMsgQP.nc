#include "Msg_Queue_Format.h"
#include <string.h>


#define TX_MSG_QUEUE_STATUS_REG		0x00C1 // 0x00A1+0x20 
#define MQ_MSG_BUSY					0
#define MQ_MSG_TRANSFERING			1


#define TX_MSG_QUEUE_DEST_REG		0x00C2 // 0x00A2+0x20 //Start transfer by putting address of destination in here 


#define TX_MSG_QUEUE_ALLOC			0x00C3 // 0x00A3+0x20
#define MQ_MSG_SUCCESS				0


#define TX_MSG_QUEUE_MSG			0x00C4 // 0x00A4+0x20 // - 0xC3  Msg buffer start address, 32 bytes long


#define MIN(a,b) (((a)<(b))?(a):(b))


module HWTXMsgQP
{
    provides interface TXMsgDest;
}
implementation
{
	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	char HW_Memory_Allocation()
	{
//		return *( (char*) TX_MSG_QUEUE_ALLOC );

		volatile char* p_temp;
		p_temp = (char*) TX_MSG_QUEUE_ALLOC;
		return *p_temp;
	}

	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	void Initiate_Transfer(nx_uint8_t destination)
	{
//		(*((nx_uint8_t*) TX_MSG_QUEUE_CONTROL_REG)) = destination;

		volatile char* p_temp;
		p_temp = (char*) TX_MSG_QUEUE_DEST_REG;
		*p_temp = destination;
	}


	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	command error_t TXMsgDest.send( nx_uint8_t	dest_ID,		//destination of message (ID of receiver buffer NOT CORE!)
								MsgID_t*		p_msg_ID, 
								nx_uint8_t*		p_data,
								unsigned short	data_size )
    {
		volatile Msg_Queue_Format_t*	p_msg;

        // Memory mapped msg FIFOs
        //1. Allocate memory for msg (HW supported operation)
		if ( !HW_Memory_Allocation() )
			return EBUSY; //or FAIL, both defined in TinyError.h

        //2. Copy msg into allocated buffer
		p_msg = (Msg_Queue_Format_t*) TX_MSG_QUEUE_MSG;
		memcpy( (void*) &(p_msg->header),	(void*) p_msg_ID,	sizeof(MsgID_t) );
		memcpy( (void*) p_msg->data,			(void*) p_data,		MIN(data_size, MSG_DATA_SIZE) );

        //3. Initiate transfer
		Initiate_Transfer(dest_ID);

        //(4. Make sure transfer succeeded - potential deadlock!)
    
        return SUCCESS; 
    }
}
