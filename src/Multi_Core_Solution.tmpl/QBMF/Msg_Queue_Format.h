#if defined(MSG_QUEUE_FORMAT_H)
#else
#define MSG_QUEUE_FORMAT_H


#define MSG_DATA_SIZE		29	// = 32-3 in bytes 
#define POLL_PERIOD			25	// in milli secs


typedef nx_struct MsgID_s
{
	nx_uint8_t	compID;		//the component this message was sent from
	nx_uint8_t	interfID;	//the interface of the component this message was sent from
	nx_uint8_t	funcID;		//the command or event of the interface this message was sent from
} MsgID_t;


typedef nx_struct Msg_Queue_Format_s 
{
	MsgID_t			header;
	nx_uint8_t		data[MSG_DATA_SIZE];
} Msg_Queue_Format_t;

#endif