interface TXMsgDest
{
	command error_t send( nx_uint8_t	dest_ID,		//destination of message (ID of receiver buffer NOT CORE!)
						MsgID_t*		p_msg_ID, 
						nx_uint8_t*		p_data,
						unsigned short	data_size );
}
