#if defined(MSG_QUEUE_ID_H)
#else
#define MSG_QUEUE_ID_H

enum
{
	<#list seasoning.data.cores.core as core>
	CORE_${core.@name?upper_case}	= ${core.@number}<#if core_has_next>,</#if>
	</#list>
};

#endif
