#include "Msg_Queue_Format.h"
#include <string.h>


#define RX_MSG_QUEUE_STATUS_REG		0x009E // 0x007E+0x20
#define MQ_MSG_RDY					0


#define RX_MSG_QUEUE_CONTROL_REG	0x009F // 0x007F+0x20
#define MQ_MSG_POP					0


#define RX_MSG_QUEUE_MSG_SOURCE_REG	0x00A0 // 0x0080+0x20


#define RX_MSG_QUEUE_MSG			0x00A1 // 0x0081+0x20	// - 0x00A0  Msg buffer start address, 32 bytes long


module HWRXMsgQP
{
    provides interface RXMsgSource;
    provides interface RXMsgMisc;
//TODO interrupt handler
//	uses interface interrupt handler
}
implementation
{
	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	char New_Msg_Available()
	{
//		return (char) ( *((nx_uint8_t*) RX_MSG_QUEUE_STATUS_REG) & (1 < MQ_MSG_RDY) );

		/*
		char temp;
		memcpy( (void*) &temp, (void*) RX_MSG_QUEUE_STATUS_REG,	sizeof(char) );
		temp &= (1 < MQ_MSG_RDY);
		return temp;
		*/

		/*
		volatile char* p_temp;
		char temp;
		p_temp = (char*) RX_MSG_QUEUE_STATUS_REG;
		memcpy( (void*) &temp, (void*) p_temp,	sizeof(char) );
		temp &= (1 < MQ_MSG_RDY);
		return temp;
		*/

		volatile char* p_temp;
		p_temp = (char*) RX_MSG_QUEUE_STATUS_REG;
		return *p_temp & (1 << MQ_MSG_RDY);
	}

	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	void Remove_Msg()
	{
//		(*((nx_uint8_t*) RX_MSG_QUEUE_CONTROL_REG)) = (nx_uint8_t) (1 < MQ_MSG_POP);

		volatile char* p_temp;
		p_temp = (char*) RX_MSG_QUEUE_CONTROL_REG;
//		*p_temp = ( ((nx_uint8_t) 1) < MQ_MSG_POP );
		*p_temp = 1 << MQ_MSG_POP;
	}

	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
    task void Handle_FIFO()
    {
		Msg_Queue_Format_t	msg;
        error_t				err;
		nx_uint8_t			source_ID;

		volatile char* p_temp;

		/*
		char			temp;
		temp = *((char*) RX_MSG_QUEUE_STATUS_REG);
		temp += *((char*) RX_MSG_QUEUE_MSG_SOURCE_REG);
		temp += *((char*) RX_MSG_QUEUE_MSG);
		memcpy( (void*) &msg,		(void*) RX_MSG_QUEUE_MSG,				sizeof(Msg_Queue_Format_t) );
		memcpy( (void*) &source_ID, (void*) RX_MSG_QUEUE_MSG_SOURCE_REG,	sizeof(nx_uint8_t) );
		memcpy( (void*) &temp,		(void*) RX_MSG_QUEUE_MSG_SOURCE_REG,	sizeof(nx_uint8_t) );
		if ( temp == 0xFF )
			return;
		*/	

		//Is there a new msg waiting?
		if ( !New_Msg_Available() )
		{
			signal RXMsgMisc.No_New_Msg();
            return;
		}

        //1. Take first msg from FIFO
		p_temp = (char*) RX_MSG_QUEUE_MSG;
		memcpy( (void*) &msg,		(void*) p_temp,	sizeof(Msg_Queue_Format_t) );

		p_temp = (char*) RX_MSG_QUEUE_MSG_SOURCE_REG;
		memcpy( (void*) &source_ID, (void*) p_temp,	sizeof(nx_uint8_t) );

        //2. Remove msg from FIFO
		Remove_Msg();

        //3. Signal the arrival of new msg 
        err = SUCCESS;
        signal RXMsgSource.receive( err,
							source_ID,
							(MsgID_t*) &(msg.header), 
							(nx_uint8_t*) msg.data, 
							MSG_DATA_SIZE );
        
        //4. Is there a new msg waiting?
		if ( !New_Msg_Available() )
		{
			signal RXMsgMisc.No_New_Msg();
            return;
		}

        post Handle_FIFO();
    }

	////////////////////////////////////////////////
	//
	//
	//
	////////////////////////////////////////////////
	command void RXMsgMisc.Post_Msg_Queue_check()
	{
		post Handle_FIFO();
	}

//TODO interrupt handler
/*
    async event void incomingFIFOinterrupt()
    {
        post handleFIFO();    
    }
*/
}

