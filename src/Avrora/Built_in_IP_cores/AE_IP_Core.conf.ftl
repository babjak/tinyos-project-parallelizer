<#assign interface_of_IP_core=seasoning["data/components/component[@as_interface_of_IP_core='AE_core']"]>
<#assign interface_of_IP_core_name=interface_of_IP_core.@name>

<#assign ref = interface_of_IP_core.QBMF_interfaces.interface.@ref>
<#assign interf = seasoning["data/interfaces/interface[ @from_ref = '" + ref + "' or @to_ref = '" + ref + "' ]"]>
<#assign other_comp_name = seasoning["data/components/component[QBMF_interfaces/interface/@ref = '" + interf.@from_ref + "' or QBMF_interfaces/interface/@ref = '" + interf.@to_ref + "' and not( @name = '" + interface_of_IP_core_name + "' ) ]/@name"]>
<#assign core_number = seasoning["data/cores/core[ local_QBMF_components/component/@name = '" + other_comp_name + "' ]/@number"]>
CORE=${core_number}

<#if interface_of_IP_core_name?is_string>
	<#list seasoning.data.cores.core.local_QBMF_components.component as comp>
		<#if comp.@name == interface_of_IP_core_name>
COMP=${comp_index}
		</#if>
	</#list>

	<#list seasoning["data/components/component/QBMF_interfaces/interface[@uses='False' and not(@name_from=preceding::interface/@name_from) and not(@name_to=preceding::interface/@name_to)]"] as interf>
		<#if seasoning["data/components/component[QBMF_interfaces/interface/@ref = '" + interf.@ref + "']/@name"] == interface_of_IP_core_name>
INTERF=${interf_index}
		</#if>
	</#list>

<#--
	<#list seasoning["data/interfaces/interface/commands/command[ not(@name=preceding::command/@name) ]/@name"] as command_name>
		<#list seasoning["data/interfaces/interface[commands/command/@name = '" + command_name + "' ]"] as interf>
			<#assign from_ref=interf.@from_ref>
			<#assign to_ref=interf.@to_ref>
			<#list interface_of_IP_core.QBMF_interfaces.interface.@ref as ref>
				<#if from_ref == ref || to_ref == ref>
COM_${command_name?upper_case}=${command_name_index}
				</#if>
			</#list>
		</#list>
	</#list>
-->

	<#list seasoning["data/interfaces/interface/events/event[ not(@name=preceding::event/@name) ]/@name"] as event_name>
		<#list seasoning["data/interfaces/interface[events/event/@name = '" + event_name + "' ]"] as interf>
			<#assign from_ref=interf.@from_ref>
			<#assign to_ref=interf.@to_ref>
			<#list interface_of_IP_core.QBMF_interfaces.interface.@ref as ref>
				<#if from_ref == ref || to_ref == ref>
SIG_${event_name?upper_case}=${event_name_index}
				</#if>
			</#list>
		</#list>
	</#list>

<#else>
	<@pp.dropOutputFile />
</#if>
