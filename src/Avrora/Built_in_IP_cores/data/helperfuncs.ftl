<#function Get_Interface_Designator interf_ref>
	<#assign interf = seasoning["data/interfaces/interface[@from_ref = $interf_ref or @to_ref = $interf_ref]"]>
	<#if interf.@name_to == interf.@name_from>
		<#return "INTERF_" + interf.@name_to?upper_case>
	<#else>
		<#assign from_interf_ref	= interf.@from_ref>
		<#assign to_interf_ref 		= interf.@to_ref>
		<#assign from_comp_name		= seasoning["data/components/component[QBMF_interfaces/interface/@ref = $from_interf_ref]/@name"]>
		<#assign to_comp_name		= seasoning["data/components/component[QBMF_interfaces/interface/@ref = $to_interf_ref]/@name"]>
		<#return "INTERF_" + from_comp_name?upper_case + "_" + interf.@name_from?upper_case + "_" + to_comp_name?upper_case + "_" + interf.@name_to?upper_case>
	</#if>
</#function>

<#function Do_Sets_Intersect x_list y_list>
	<#list x_list as x>
		<#list y_list as y>
			<#if x == y>
				<#return true>
			</#if>
		</#list>
	</#list>
 	<#return false>
</#function>
